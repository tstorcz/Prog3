﻿using System;

namespace StructValueType
{
    struct PolarPoint
    {
        public int r;
        public int fi;
    }

    struct EucledianPoint
    {
        public int x;
        private int y;
        public readonly int z;          // visible, but can not be modified

        public EucledianPoint(int x, int y, int z)
        {
            this.x = x;
            this.y = y;
            this.z = z;
        }
    }

    class Program
    {
        static void Main(string[] args)     //Entry point of the program execution
        {
            PolarPoint p1;          //no new operator, handled as simple type with default constructor
            p1.r = 5; p1.fi = 7;
            Console.WriteLine("r={0}; fi={1}", p1.r, p1.fi);
            PolarPoint p2 = p1;     //content of a value type will be copied
            Console.WriteLine("r={0}; fi={1}", p2.r, p2.fi);
            p2.r += 4;              //only content of p1 has changed
            Console.WriteLine("r={0}; fi={1}", p2.r, p2.fi);
            Console.WriteLine("r={0}; fi={1}", p1.r, p1.fi);


            EucledianPoint a = new EucledianPoint(10, 5, 8);
            // using parametric constructor, new operator is required, still value type
            Console.WriteLine("X={0}; Y={1}", a.x, a.z);
            EucledianPoint b = a;   //content of a value type will be copied
            Console.WriteLine("X={0}; Y={1}", b.x, b.z);
            b.x += 5;               //only content of b has changed
            Console.WriteLine("X={0}; Y={1}", b.x, b.z);
            Console.WriteLine("X={0}; Y={1}", a.x, a.z);
        }
    }
}
