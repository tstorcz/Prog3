﻿using System;

namespace ValueVsReference
{
    internal class Program
    {
        private static int M(int p)
        {
            p = p + 1;
            return p;
        }

        private static int M2(ref int p)
        {
            p = p + 1;
            return p;
        }

        static void Main(string[] args)
        {
            int v = 22;

            for (int i = 0; i < 10; i++)
            {
                M(v);
            }
            Console.WriteLine(v);

            for (int i = 0; i < 10; i++)
            {
                M2(ref v);
            }
            Console.WriteLine(v);
        }
    }
}
