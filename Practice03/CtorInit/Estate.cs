﻿using System;

namespace CtorInit
{
    class Estate
    {
        static public int MinimumPrice { get; set; } = 300000;

        static Estate()
        {
            MinimumPrice = 200000;
            Console.WriteLine("static ctor");
        }

        public string Type { get; private set; }
        public int Rooms { get; private set; }
        public bool HasGarden { get; private set; }
        public bool HasGarage { get; private set; }
        public bool ShopNearby { get; set; }

        public Estate(string type, int rooms)
        {
            Type = type;
            Rooms = rooms;
        }

        public Estate(string type, int rooms, bool garden = false, bool garage = false) 
            : this(type, rooms)
        {
            HasGarden = garden;
            HasGarage = garage;

            //In the example initialized on construction
            //ShopNearby = false;       //Type default, no need to explicitly set
        }

        public Estate(string type, int rooms, bool shopNearby) : this(type, rooms)
        {
            ShopNearby = shopNearby;
        }

        public override string ToString()
        {
            return string.Format("This {0} has {1} rooms {2} garden, {3} garage, {4} shop nearby", 
                Type, Rooms,
                HasGarden ? "with" : "without",
                HasGarage ? "with" : "without",
                ShopNearby ? "with" : "without"
            );
        }
    }
}
