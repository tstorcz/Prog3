﻿using System;

namespace CtorInit
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("The program starts here");

            Program p = new Program();

            Console.WriteLine("Program instance has been created");

            Console.WriteLine("Minimum before instance: {0}", Estate.MinimumPrice);

            Estate flat1 = new Estate("flat", 3)
            {
                ShopNearby = true
            };

            Console.WriteLine("Minimum after instance: {0}", Estate.MinimumPrice);


            Console.WriteLine();

            Estate house1 = new Estate("house", 5, garage: true);
            Estate cottage1 = new Estate("summer house", 2, garden: true)
            {
                ShopNearby = true
            };

            Estate house2 = new Estate("house", 6, garden: true, garage: true);
            Estate house3 = new Estate("house", 7, true, true)
            {
                ShopNearby = true
            };

            Console.WriteLine(flat1);
            Console.WriteLine(house1);
            Console.WriteLine(cottage1);
            Console.WriteLine(house2);
            Console.WriteLine(house3);


            Console.WriteLine();

            //Which constructor has been called?
            Estate unclear = new Estate("Unclearly created estate", 5, true);
            Console.WriteLine(unclear);

            Console.WriteLine();
        }
    }
}
