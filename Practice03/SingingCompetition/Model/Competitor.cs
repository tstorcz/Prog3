﻿using System;

namespace SingingCompetition.Model
{
    class Competitor
    {
        private static int nextId;

        static Competitor()
        {
            nextId = 1;
        }

        public int Id { get; private set; }
        public string Name { get; private set; }
        public string Specialization { get; private set; }

        private int points;
        public int Points {
            get {
                return points;
            }
            set {
                if(points < value)
                {
                    points = value;
                }
                else
                {
                    throw new Exception("Can not decrement points");
                }
            }
        }

        public Competitor(int id, string name, string spec)
        {
            Id = id;
            Name = name;
            Specialization = spec;
            points = 0;
        }

        public Competitor(string name, string spec) : this(nextId++, name, spec)
        {
        }

        public void AddPoints(int newPoints)
        {
            if (newPoints > 0)
            {
                points += newPoints;
            }
            else
            {
                throw new Exception("Point change can not be less than 1");
            }
        }

        public override string ToString()
        {
            return String.Format("Id: #{0}, Name: {1}, Points: {2}", Id, Name, Points);
        }
    }
}
