﻿using System;

namespace _00_OOP_tools
{
    //Class is still abstract, because cutters of specific make and model exist
    public abstract class Cutter : Machine
    {
        //Constructor calls parent (base) constructor
        public Cutter(int height, int width, int length) : base(height, width, length)
        {
            
        }

        //Creation method is more specific
        public override void CreateWorkpiece()
        {
            Console.WriteLine("Cut workpiece from base material");
        }
    }
}
