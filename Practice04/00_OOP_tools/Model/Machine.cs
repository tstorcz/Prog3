﻿using System;

namespace _00_OOP_tools
{
    //Abstract class, because no "machine" exists in the storage
    public abstract class Machine
    {
        public int WorkPieceHeight { get; private set; }
        public int WorkPieceWidth { get; private set; }
        public int WorkPieceLength { get; private set; }

        public int WorkPieceVolume
        {
            get {
                return WorkPieceHeight * WorkPieceLength * WorkPieceWidth;
            }
        }

        public Machine(int width, int length, int height)
        {
            this.WorkPieceHeight = height;
            this.WorkPieceLength = length;
            this.WorkPieceWidth = width;
        }

        //Machine uses an unknown work method
        public abstract void CreateWorkpiece();

        //Abstract class also can contain implemented (non-abstract) method
        public void Reset()
        {
            Console.WriteLine("Head moved to initial position");
        }
    }
}
