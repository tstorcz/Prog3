﻿using System;

namespace _00_OOP_tools
{
    class MetalPrinter : Printer
    {
        //Constructor calls parent (base) constructor
        public MetalPrinter(int height, int width, int length) : base(height, width, length)
        {
            //Empty method body
        }

        //Creation method is even more specific
        //Inherited behaviour is redefined
        public new void CreateWorkpiece()
        {
            Console.WriteLine("Build workpiece from metal");
        }
    }
}
