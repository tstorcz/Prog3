﻿using System;

namespace _00_OOP_tools
{
    class PlasticPrinter : Printer
    {
        //Constructor calls parent (base) constructor
        public PlasticPrinter(int height, int width, int length) : base(height, width, length)
        {
            //Empty method body
        }

        //Creation method is even more specific
        //Inherited behaviour is redefined
        public new void CreateWorkpiece()
        {
            Console.WriteLine("Build workpiece from plastic");
        }

        public new void Reset()
        {
            base.Reset();
            Console.WriteLine("Head cooled down");
        }
    }
}
