﻿using System;

namespace CatDogContest.Model
{
    class Cat : Animal
    {
        public bool HasCarrier { get; set; }

        public Cat(int id, string name, int birthYear, bool hasCarrier) : base(id, name, birthYear)
        {
            HasCarrier = hasCarrier;
        }

        override public int TotalPoints
        {
            get
            {
                return HasCarrier ? base.TotalPoints : 0;
            }
        }

        public override string ToString()
        {
            if (!isEvaluated)
            {
                return String.Format("A cat named: {0}, with id: #{1} has not been evaluated", Name, Id);
            }
            else
            {
                return String.Format("A cat named: {0}, with id: #{1} has {2} points", Name, Id, TotalPoints);
            }
        }
    }
}
