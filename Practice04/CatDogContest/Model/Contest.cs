﻿using System;
using System.Collections.Generic;

namespace CatDogContest.Model
{
    class Contest
    {
        #region Contest instance specific MaxAge
        //This instance property is used when different MaxAge must be used for 
        //different Contest instances
        //
        //Value of maxage is checked in the setter, therefore explicit getter and setter were created.
        //Important: the value can not be set from outside the object -> setter is private
        private int maxAge;
        public int MaxAge {
            get { return maxAge; }
            private set {
                if (value < 1) throw new Exception("Maximum age is too small");
                maxAge = value;
            }
        }

        // Using local MaxAge data instead of class data of Animal
        // compued data aquisition is not in class
        // this.GetAnimalPoints(animal) must be used instead of property getter
        private int GetAnimalPoints(Animal animal)
        {
            if (animal.Age > maxAge)
            {
                return 0;
            }
            else
            {
                return (maxAge - animal.Age) * animal.BeautyPoints +
                    animal.Age * animal.BehaviourPoints;
            }
        }

        #endregion

        //Points have to be set in pair, so use SetPointLimits setter to check their relations
        public int PointsMin { get; private set; }
        public int PointsMax { get; private set; }


        private List<Animal> competitors;

        public List<Animal> Competitors
        {
            get { return competitors.GetRange(0, competitors.Count); }
        }

        private Random random;

        private void SetPointLimits(int pointsMin, int pointsMax)
        {
            if (pointsMin < 1) throw new Exception("Minimum point is too small");
            if (pointsMax < pointsMin) throw new Exception("Maximum point is too small");
            PointsMin = pointsMin;
            PointsMax = pointsMax;
        }

        public Contest(int pointsMin, int pointsMax, int maxAge)
        {
            SetPointLimits(pointsMin, pointsMax);

            MaxAge = maxAge;

            competitors = new List<Animal>();
            random = new Random();
        }

        public void RegistrationFromFile(FileParser parser, string fileName)
        {
            competitors.AddRange(parser.ParseFile(fileName));
        }

        public void EvaluateCompetitors()
        {
            int beautyPoints, behaviourPoints;
            foreach (Animal competitor in competitors)
            {
                beautyPoints = random.Next(PointsMin, PointsMax+1);
                behaviourPoints = random.Next(PointsMin, PointsMax+1);
                Console.WriteLine(
                        "Evaluation of competitor #{0}: beauty={1} behaviour={2}",
                        competitor.Id, beautyPoints, behaviourPoints
                    );
                competitor.Evaluation(beautyPoints, behaviourPoints);
            }
        }

        #region List filters

        private Animal GetOneWinner()
        {
            Animal winner = competitors[0];
            foreach(Animal competitor in competitors)
            {
                if(winner.TotalPoints < competitor.TotalPoints)
                {
                    winner = competitor;
                }
            }

            return winner;
        }

        private List<Animal> FindWinnersByPoints(int points)
        {
            List<Animal> competitorsFound = new List<Animal>();
            foreach (Animal competitor in competitors)
            {
                if (points == competitor.TotalPoints)
                {
                    competitorsFound.Add(competitor);
                }
            }

            return competitorsFound;
        }

        public Animal Find(int id)
        {
            return competitors.Find(x => x.Id == id);
        }

        //Get ALL competitors who are tied for the lead
        //Read the list only 1 time
        //The list is not sorted (have to read the whole list)
        public List<Animal> GetAllWinners1()
        {
            List<Animal> winners = new List<Animal>();
            foreach (Animal competitor in competitors)
            {
                //If no winner stored or a better item found, clear the list
                if (winners.Count > 0 && winners[0].TotalPoints < competitor.TotalPoints)
                {
                    winners.Clear();
                }

                //If no winner stored or tied with first winner, store the new winner
                if (winners.Count == 0 || winners[0].TotalPoints == competitor.TotalPoints)
                {
                    winners.Add(competitor);
                }
            }

            return winners;
        }

        public List<Animal> GetAllWinners2()
        {
            return FindWinnersByPoints(GetOneWinner().TotalPoints);
        }

        #endregion

        #region Sort

        public void SortByPoints()
        {
            competitors.Sort((x, y) => -x.TotalPoints.CompareTo(y.TotalPoints));
        }

        public void SortById()
        {
            competitors.Sort((x, y) => x.Id.CompareTo(y.Id));
        }

        #endregion
        
    }
}
