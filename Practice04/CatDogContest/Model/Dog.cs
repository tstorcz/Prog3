﻿using System;

namespace CatDogContest.Model
{
    class Dog : Animal
    {
        public int RelationToKeeper { get; private set; }

        public Dog(int id, string name, int birthYear, 
            int relationToKeeper) 
            : base(id, name, birthYear)
        {
            RelationToKeeper = relationToKeeper;
        }

        override public int TotalPoints
        {
            get
            {
                return base.TotalPoints + RelationToKeeper;
            }
        }

        public override string ToString()
        {
            if (!isEvaluated)
            {
                return String.Format("A dog named: {0}, with id: #{1} has not been evaluated", Name, Id);
            }
            else
            {
                return String.Format("A dog named: {0}, with id: #{1} has {2} points", Name, Id, TotalPoints);
            }
        }
    }
}
