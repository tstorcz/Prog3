﻿using System;
using System.Collections.Generic;
using System.IO;

namespace CatDogContest.Model
{
    class FileParser4LinesMultiClass : FileParser
    {
        private Dictionary<
            string, Func<int, string, string, string, Animal>
            > factories;

        public FileParser4LinesMultiClass()
        {
            factories = new Dictionary<
                string, Func<int, string, string, string, Animal>
                >();
        }

        public void AddFactory(
            string typeString,
            Func<int, string, string, string, Animal> factoryMethod
            )
        {
            factories[typeString] = factoryMethod;
        }

        public override List<Animal> ParseFile(string fileName)
        {
            List<Animal> competitors = new List<Animal>();
            StreamReader reader = new StreamReader(fileName);

            while (!reader.EndOfStream)
            {
                competitors.Add(
                    factories[reader.ReadLine()](
                        competitors.Count + 1,
                        reader.ReadLine(),
                        reader.ReadLine(),
                        reader.ReadLine()
                    )
                );
            }

            reader.Close();

            return competitors;
        }
    }
}
