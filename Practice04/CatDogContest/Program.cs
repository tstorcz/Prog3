﻿using CatDogContest.Model;
using System;
using System.Collections.Generic;

namespace CatDogContest
{
    class Program
    {
        private const int POINTS_MIN = 1;
        private const int POINTS_MAX = 10;

        private const int MAX_AGE = 10;
        private const int CURRENT_YEAR = 2020;

        static void Main(string[] args)
        {
            Animal.Init(MAX_AGE, CURRENT_YEAR);

            Contest contest = new Contest(POINTS_MIN, POINTS_MAX, MAX_AGE);



            Console.WriteLine("----==== Registration ====----");
            Registration1(contest);
            //Registration2(contest);



            ListCompetitors(contest.Competitors);

            Console.WriteLine("----==== Evaluation ====----");
            contest.EvaluateCompetitors();

            Console.WriteLine("----==== Current standing ====----");
            ListCompetitors(contest.Competitors);

            Console.WriteLine("----==== Winners (1) ====----");
            ListCompetitors(contest.GetAllWinners1());

            Console.WriteLine("----==== Winners (2) ====----");
            ListCompetitors(contest.GetAllWinners2());

            Console.WriteLine("----==== Final result ====----");
            contest.SortByPoints();
            ListCompetitors(contest.Competitors);
        }

        private static void Registration1(Contest contest)
        {
            contest.RegistrationFromFile(
                new FileParser4Line(), "..\\..\\allatok.txt"
             );
        }

        private static void Registration2(Contest contest)
        {
            FileParser4LinesMultiClass parser = new FileParser4LinesMultiClass();
            parser.AddFactory(
                "kutya",
                (id, l1, l2, l3) =>
                    new Dog(id, l1, Convert.ToInt32(l2), Convert.ToInt32(l3))
            );

            parser.AddFactory(
                "macska",
                (id, l1, l2, l3) =>
                    new Cat(id, l1, Convert.ToInt32(l2), l3 == "true")
            );

            contest.RegistrationFromFile(
                parser, "..\\..\\allatok.txt"
             );
        }

        private static void ListCompetitors(List<Animal> competitorsToList)
        {
            foreach (Animal competitor in competitorsToList)
            {
                Console.WriteLine(competitor);
            }
        }
    }
}
