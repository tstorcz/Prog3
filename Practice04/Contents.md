# Contents of Practice 04  
In this lecture you see how to:
- implement inheritance
- declare and use virtual, override, sealed methods
- create and use abstract methods and classes
- use behaviour modification with type casts
  
## 00 - OOP tools  
Class hierarchy:  
- Machine  
  - Cutter  
    - CNCMilling
    - LaserCutter
  - Printer
    - MetalPrinter
    - PlasticPrinter
  
Machine class declares workspace size parameters as properties, CreateWorkPiece and Reset behaviours.  
Through the inheritance, CreateWorkPiece behaviour is modified:
- Cutters specialize (override) the behaviour (functionally confirm Liskov substitution principal)
- Printers change (new) the behaviour (sintactically confirm Liskov substitution principal)
  
This difference is visible when casting references between classes.
  
## 01 - Cat & Dog contest
The task is to read animals (cats and dogs) with different properties from a file, then evaluate their behaviour and beauty. From partial evaluation calculate the final result of each competitors, then generate the result of the competition.  
The solution is initiated from Animal Contest of Practice 02, but enhanced with Sing Competition of Practice 03.  
Now competitors are divided to `Cat` and `Dog` classes, inherited from `Animal`. Competitors are stored in one composed list storage of `Animal` instances.

List of competitors loaded from a text file.  
BUT: parse of a text file of special structure should be encapsulated in a specific class.  
To solve:
- create an abstract class providing `ParseFile` virtual behaviour (override enabled)
- create a specific parser `FileParser3Line` inherited from `ParseFile` implementing the specific line interpretation
- `Contest.RegistrationFromFile` receives a specific parser instance and a file name to parse

Interesting parts of `Contest` class:
- Value validation of setting instance properties
- `GetAllWinners` finds ALL winners in an unsorted list reading it only ones
- List competitor collection using one single output method `ListCompetitors` with different paramteres

  
## 03 - Shells
`Cylinder` is a 3D geometrical shape, **does not exsist in real** life -> it is an ==abstraction==  
`Rod`: it has material and shape, it is a manifestation of the cylinder -> exists in real life  
`Tube`: a special `Rod`, which has a `Rod` hole in the center.  
  
Interesting:
- `Cylinder` declares `Volume`
- `Rod` declares `Mass` which uses `Volume` declared in `Cylinder`
- `Tube` overrides `Volume`, result of `Tube.Mass` also changed  
even if `Tube.Mass` is indeed `Rod.Mass` and that uses `Cylinder.Volume`  
but becuase of `Volume` override all references of `Volume` through `Tube` class points to the overridden behaviour.