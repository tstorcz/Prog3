﻿using System;

namespace Shells.Model
{
    class Tube : Rod
    {
        public int Thickness { get; set; }

        public override double Volume
        {
            get
            {
                if (Thickness > 0)
                {
                    return base.Volume - new Rod(Radius - Thickness, Height, Density).Volume;
                }
                else return 0;
            }
        }

        public Tube(int r, int h, int d, int t) : base(r, h, d)
        {
            this.Thickness = t;
        }

        public override string ToString()
        {
            return String.Format("Tube radius : {0}, height: {1}, volume: {2:0.00}, weight: {3:0.00}", 
                Radius, Height, Volume, Mass
            );
        }
    }
}
