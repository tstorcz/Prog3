﻿using System;
using System.Collections.Generic;

namespace _00_a_OperatorOverload
{
    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    //Example of wrong interpretation of operators and indexer
    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    class Player
    {
        //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        //Overload + operator to extend competences of a player with competences of an other (add competences)
        //Wrong implementation, modifies a parameter instead of creating a new instance
        public static Player operator +(Player a, Player b) {
            for (int i = 0; i < b.CompetenceCount; i++)
                a.AddCompetence(b[i]);

            //!!!!WRONG!!!!!
            return a;
        }

        public string Name { get; private set; }

        //Salary can be changed with post
        public string Post { get; set; }
        public int Salary { get; set; }

        private List<string> competences;

        //Declaring an indexer, to access competences directly
        //Indexer is NOT an operator, does NOT require operator keyword
        //Works with instance state (data), therefore NOT static
        //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        //Indexer is not the best choice to access a specific property by index
        public string this[int i]
        {
            get {
                if (i >= 0 && i < competences.Count)
                    return competences[i];
                else
                    throw new IndexOutOfRangeException();
            }
        }

        //Computed property to return number of competences
        public int CompetenceCount { get { return competences.Count; } }

        public Player(string name, string post, int salary)
        {
            Name = name;
            Post = post;
            Salary = salary;
            competences = new List<string>();
        }

        public void AddCompetence(string newCompetence)
        {
            competences.Add(newCompetence);
        }

        public override string ToString()
        {
            return String.Format("{0}: {1}", Name, String.Join(", ", competences));
        }
    }
}
