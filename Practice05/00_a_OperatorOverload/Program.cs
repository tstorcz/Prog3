﻿using System;

namespace _00_a_OperatorOverload
{
    class Program
    {
        static void Main(string[] args)
        {
            //Create 2 instances of custom class
            Vector3D v1 = new Vector3D(2, 3, 4);
            Vector3D v2 = new Vector3D(5, 7, 12);

            //Apply a predefined, but redefined operator on instances
            Vector3D v3 = v1 + v2;

            //Print the result
            Console.WriteLine(v1);
            Console.WriteLine(v2);
            Console.WriteLine(v3);
            //Operator also can be applies as a common expression
            Console.WriteLine(v1 - v2);



            //Create a custom list to store Player instances (players)
            PlayerList playerList = new PlayerList();

            //Create and store player, set his competences
            Player eric = new Player("Eric", "defender", 80);
            playerList.AddPlayer(eric);
            eric.AddCompetence("speed");

            //Create and store new player
            Player mathias = new Player("Mathias", "defender", 85);
            playerList.AddPlayer(mathias);

            //Set competences
            mathias.AddCompetence("head play");
            //Mathias knows everything what Erik knows and more
            mathias = mathias + eric;

            //Create and store new player
            Player dominic = new Player("Dominic", "midfilder", 100);
            playerList.AddPlayer(dominic);

            //Set competences
            dominic.AddCompetence("pass accuracy");

            //Problem with wrong implementation of addition
            dominic = eric + mathias;                   //Dominic knows more than Eric and Mathias
                                                        //BUT instead it modifies and returns Eric
                                                        //reference of Dominic has been lost!!!

            Console.WriteLine("--------======== Operator ========--------");

            //Who knows more? Eric or Mathias
            ListCompetences(eric);
            ListCompetences(mathias);

            //!!!!!!!!!!!!!!!!!!!!!!!
            ListCompetences(dominic);                   //PRINTS modified Eric, instead of Dominic
            //!!!!!!!!!!!!!!!!!!!!!!!

            Console.WriteLine("--------======== List ========--------");

            //List competences of players
            for (int i = 0; i < playerList.Count; i++)
            {
                //Usage of indexer is appropriate here to refer to i-th player of the list
                ListCompetences(playerList[i]);
            }

            Console.WriteLine(playerList["Mathias"]);
        }

        private static void ListCompetences(Player player)
        {
            Console.WriteLine("Competences of {0} ({1}):", player.Name, player.Post);
            for(int i = 0; i < player.CompetenceCount; i++)
            {
                //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                //Decreased readability, palyer[i] usually means i-th player,
                //not i-th property of a player
                Console.WriteLine("\t{0}", player[i]);
            }
        }
    }
}
