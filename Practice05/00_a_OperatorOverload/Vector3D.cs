﻿using System;

namespace _00_a_OperatorOverload
{
    class Vector3D
    {
        //Automatic properties can be read, but can NOT be modified, set by the constructor
        public int x { get; private set; }
        public int y { get; private set; }
        public int z { get; private set; }

        //Computed property
        public double Lenght
        {
            get
            {
                return Math.Sqrt(x * x + y * y + z * z);
            }
        }

        public Vector3D(int x, int y, int z)
        {
            this.x = x;
            this.y = y;
            this.z = z;
        }

        public override string ToString()
        {
            return String.Format("|({0}; {1}; {2})| = {3}", x, y, z, Lenght);
        }

        #region Overloaded operators

        //Egy operandusú operátorok
        public static Vector3D operator +(Vector3D a) => a;
        public static Vector3D operator -(Vector3D a) => new Vector3D(-a.x, -a.y, -a.z);

        //Két operandusú operátorok
        public static Vector3D operator +(Vector3D a, Vector3D b) => new Vector3D(a.x + b.x, a.y + b.y, a.z + b.z);
        public static Vector3D operator -(Vector3D a, Vector3D b) => a + (-b);

        #endregion
    }
}
