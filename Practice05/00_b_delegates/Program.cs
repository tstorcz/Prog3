﻿using System;

namespace _00_b_delegates
{
    class Program
    {
        //Static method to print without instance states
        static void StaticPrint()
        {
            Console.WriteLine("Printer from static");
        }

        //---------------------------------------
        //Method reference (delegate) declaration
        public delegate void Print();
        //---------------------------------------

        static void Main(string[] args)
        {
            //Create inherited instances
            Printer p = new Printer("economic");
            Printer p2 = new Printer("normal");
            ColorPrinter cp = new ColorPrinter("rich", "orange");

            //---=== Create method references

            //A reference to a static method of class Program
            Print staticPrint = StaticPrint;
            Print staticBasePrint = Printer.StaticPrint;

            //Instance method references of different instances (c, cp)
            Print basePrint = p.Print;
            Print colorPrint = cp.Print;

            //Print from anonymous method
            Print delegatePrint = delegate () { Console.WriteLine("Anonymous print"); };

            //Action is a reference of a method without return type
            Action actionPrint = delegate () { Console.WriteLine("Action print"); };

            //Func<int, int, int> add = (a, b) => a + b;

            //Print from lambda expression
            Print lambdaPrint = () => Console.WriteLine("Lambda print");

            //---=== Call methods through method references

            //Accecssing static method through delegate instance
            staticPrint();

            //Accessing instance method through delegate instance
            basePrint();

            //Accessing method of inderited instance, using implicit cast
            colorPrint();
            //changing instance state
            cp.Color = "blue";
            //Instance state change has been applied when calling method through delegate instance
            //Indeed: delegate instance refers to instance method, 
            //        which operates on current instance state
            colorPrint();

            //Access anonymous method through delegate instance
            delegatePrint();

            //Access anonymous method through Action instance
            actionPrint();

            //Access lambda expression through delegate instance
            lambdaPrint();
        }
    }
}
