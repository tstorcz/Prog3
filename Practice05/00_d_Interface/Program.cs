﻿namespace _00_d_Interface
{
    internal class Program
    {
        static void Main(string[] args)
        {
            GermanShepherd myDog = new GermanShepherd();
            myDog.Eat();
            myDog.Bark();

            IDog aDog = myDog;
            aDog.Eat();
            aDog.Bark();

            IAnimal anAnimal = myDog;
            anAnimal.Eat();

            Animal anAnimal2 = myDog;
            anAnimal2.Eat();

            GermanShepherd germanShepherd;
            if (anAnimal is GermanShepherd)
            {
                germanShepherd = (GermanShepherd)anAnimal;
            }
            else
            {
                germanShepherd = null;
            }

            germanShepherd.Eat();
            germanShepherd.Bark();

            germanShepherd = anAnimal as GermanShepherd;

            germanShepherd.Eat();
            germanShepherd.Bark();
        }
    }
}
