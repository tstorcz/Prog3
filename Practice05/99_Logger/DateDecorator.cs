﻿using System;

namespace _99_Logger
{
    class DateDecorator : IMessageDecorator
    {
        private IMessageDecorator decorator;

        public DateDecorator(IMessageDecorator decorator = null)
        {
            this.decorator = decorator;
        }

        public string Decorate(string message)
        {
            return string.Format(
                "{0} {1}", 
                DateTime.Now.ToString("yyyy.MM.dd"),
                decorator == null ? message : decorator.Decorate(message)
            );
        }
    }
}
