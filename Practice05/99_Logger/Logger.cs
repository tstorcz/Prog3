﻿using System.IO;

namespace _99_Logger
{
    class Logger
    {
        public LogLevels Level { get; set; }
        public string FileName { get; private set; }

        public Logger(string fileName)
        {
            Level = LogLevels.Error;
            FileName = fileName;
        }

        public void Write(string message, LogLevels messageLevel)
        {
            if(messageLevel <= Level)
            {
                using (StreamWriter sw = File.AppendText(FileName))
                {
                    sw.WriteLine(message);
                }
            }
        }
    }
}
