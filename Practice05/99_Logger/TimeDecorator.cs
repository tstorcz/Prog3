﻿using System;

namespace _99_Logger
{
    class TimeDecorator : IMessageDecorator
    {
        private IMessageDecorator decorator;

        public TimeDecorator(IMessageDecorator decorator = null)
        {
            this.decorator = decorator;
        }

        public string Decorate(string message)
        {
            return string.Format(
                "{0} {1}", 
                DateTime.Now.ToString("HH:mm:ss"),
                decorator == null ? message : decorator.Decorate(message)
            );
        }
    }
}
