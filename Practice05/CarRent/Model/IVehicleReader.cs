﻿using System.Collections.Generic;

namespace CarRent.Model
{
    internal interface IVehicleReader
    {
        List<Vehicle> Read(string sourceName);
    }
}
