﻿namespace CarRent.Model
{
    abstract class Parser
    {
        public abstract Vehicle Parse(string source, int newId);
    }
}
