﻿using System;

namespace CarRent.Model
{
    class SingleLineVehicleParser : Parser
    {
        public override Vehicle Parse(string source, int newId)
        {
            string[] lines = source.Split(';');

            if (lines[0] == "bus")
            {
                return
                    new Bus(
                        lines[1],
                        Convert.ToInt32(lines[2]),
                        string.Format("B{0}", newId),
                        Convert.ToInt32(lines[3])
                    );
            }
            else
            {
                return
                    new Truck(
                        lines[1],
                        Convert.ToInt32(lines[2]),
                        string.Format("T{0}", newId),
                        Convert.ToDouble(lines[3])
                    );
            }

        }
    }
}

