﻿using System;

namespace CarRent
{
    class Truck : Vehicle
    {
        //Generic multiplier (coefficient) is stored in class property
        public static double TonPrice { get; set; }  = 2000;

        public double MaxWeight { get; private set; }

        //Constructor of base class is called with received parameters
        public Truck(string licence, int prodYear, string id, double maxWeight)
            : base(licence, prodYear, id)
        {
            MaxWeight = maxWeight;
        }

        //Override inherited proterty to provide more specific behavior
        //Extend Fare by truck specific components
        override public int Fare
        {
            get
            {
                return base.Fare + (int)(MaxWeight * TonPrice);
            }
        }

        public override string ToString()
        {
            //Content of base.ToString has been extended
            return String.Format("Teherautó ({0}, max.súly: {1}t, bérleti díj: {2}Ft) {3}", 
                base.ToString(), MaxWeight, Fare, IsAvailable ? "bérelhető" : "nem bérelhető");
        }
    }
}
