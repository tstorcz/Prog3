# Contents of Practice 05
In this lecture you see how to:
- declare indexer
- overload an operator
- declare, implement and use interfaces
- usa type parameters in generic methods and classes
- declare and usa enumerations
- declare and use method references, delegates, anonymous methods, and lambdas

## 00 - detailed presentation of OOP tools
### A - Operator overload (indeed override)
In the first part of the example, a class to store 3D vectors is declared.  
Indexer `[index]` can be declared for accessing items of array like objects.  
Please note the followings:
- indexer is a method, with special signature
- interpretation of index depends on indexer implementation (functional requirements)
- type of index can be specified (not only int)
- indexer can be overloaded
- class can have multiple indexers with different meaning
- ==using the indexer with different functionality makes code less readable!==
  
Mathematical operators on 3D vectors can be defined, so their implementation also can take place.  
Operations + and - are implemented, both return a new 3D vector with the properties of the result vector.
  
In the second part of an example it is presented that not only mathematical operations can be implemented.  
According to the last statement of previous chapter, **operator returns the result of the operation**, even if it is not a mathematical operation.  
1. Let's declare a class of players list of competences ("name of competence").  
2. let's define a "learn" operation:  
    - interpreted on 2 players
    - represented by + operator
    - playe1 + player2 means that player1 learns all competences that player2 has
    - return the modified player: no new player is created, but player1 is modified
3. then create players and use the new + operator
  
==Always be aware of the operator creates new object or not==  
- '+' operator of `Vector3D` works different than all others, maybe refactor is required!
- There is a difference between `Vector3D` and `Player` operator implementations. Behaviour depends on requirements!

### B - Delegates and method references,
Delegates can be declared to have a type for variables to store method references.  
Method references can be set to:
- static methods
- instance methods
- anonymous methods (no name, the only reference is the delegate variable)
- lambda expressions (short declaration of anonymous methods)
  
Delegate variables can be used as method names.  
  
### C - Generic codes & type parameter
`CheckLength` is an example of using single implementation for multiple types. But to achieve this and not to break strong typing oc C#, a usage of type parameter is required.  
So duplicated implementation (for `int` and for `float`) can be replaced by a single implementation with a type parameter.  
  
Please note the followings:
- `LongerString` method is overloaded, overloads do the same operation but on different types
- type parameter can be inferred (like in Java) from output parameter type
  
`UseStorages` part of the example uses a generic, array based storage implemented in `MultiItemStorage` generic class.  
This class shows the basics of how a generic list is implemented.  
In forther examples, a built-in generic dynamic list (`List<T>`) will be used.
  
## 01 - Rental application
Create an application which maintains a list about rentable vehicles.  
The application provides functionaliy:
- The vehicle list is read from a text file.
- User can search specific vehicle (by id or by index)
- User can filter buses/trucks to rent (available vith specific propertied)
  
Try to conform **SOLID** OOP principals.
  
### A - Rent vehicles (Bus & Truck)
Use things already known:
- same fare for all vehicles of a type (use static properties)
- enumerated values for state representation of rentable vehicles
- implementation of `IComparable` interface to support automatic sort (`List.Sort`)
- Instance of abstract class to parse file (Single responsibility)
- `VehicleCatalog` class outputs filter results, but conforming single responsobility principal, responsible only for filtering list, not outputing items  
This is done by a method declared in the controller (`Program`) and its reference is passed to `VehicleCatalog`
  
### B - Also rent drivers
Modify vehicle container of Solution A:
- filters should return lists of items found instead of outputting them
- provide an option to exetute custom operation on a vehicle given by licence plate
- implement IEnumerable interface to give an option to enumerate the storage contents
  
Extend the program with an option to rent also drivers.  
Drivers have driving license, in which the vehicle type (A, B, C, D, E, T, TR, V) is marked which a driver can drive. Certainly a driver could be able to drive more than one type of vehicles.  
Check how enum type is used to store flag-like value.
  
Drivers are also rentable, so they:
- should be stored in the same storage as vehicles
- should have same rentability related properties as vehicles (Fare, IsAvailable)
- should have the same management infrastructure as vehicles (Rent, Return)

To implement this, declare an `IRentable` interface with forementioned components, then implement in `Vehicle` and in `Driver`.  
Now a catalog based on this type parameter can store instances of both classes.
  
## 09 - Temperature statistics
As in Practice 03/07, create an application to store minimum and maximum temperature of days. From this, average temperature can be calculated.  
Store entered days, select coolest and hottest days and list average temperature of continous days.

This example you can see how to declare lambda expressiong and how to use them as method parameter.  
To achieve this, important to see how such a method parameter has to declared and used inside the method.
  