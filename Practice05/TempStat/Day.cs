﻿using System;

namespace TempStat
{
    class Day
    {
        public int MinTemp { get; private set; }
        public int MaxTemp { get; private set; }

        public int AvgTemp
        {
            get
            {
                return (MaxTemp + MinTemp) / 2;
            }
        }

        public Day(int minTemp, int maxTemp)
        {
            MinTemp = minTemp;
            MaxTemp = maxTemp;
        }

        public override string ToString()
        {
            return String.Format("min: {0}\tmax: {1}\tavg: {2}", MinTemp, MaxTemp, AvgTemp);
        }
    }
}
