﻿using System;
using System.Drawing;
using System.Windows.Forms;

//After creating a Console Application
//Do not forget to add System.Windows.Forms Assembly as reference
//And set Output Type to Windows Application is project properties

namespace _11_c_ConsoleWindowCtrl
{
    class MainForm : Form
    {
        //Controls used on the form
        private Label message;
        private Button button;

        public MainForm()
        {
            Initialize();
        }

        private void Initialize()
        {
            Size = new Size(400, 300);
            Text = "My first windows form";
            BackColor = Color.Blue;
            CenterToScreen();

            AddControls();
        }

        private void AddControls()
        {
            message = new Label();
            message.Text = "Üres";
            message.Location = new Point(30, 60);
            //Labels have automatic size according to their content
            message.ForeColor = Color.White;

            //Add label control to Controls collection of the form to show and handle it
            Controls.Add(message);

            button = new Button();
            button.Location = new Point(30, 30);
            //Buttons have automatic size
            //can be changed using button.Size property
            button.Text = "Nyomd meg";
            button.BackColor = Color.Yellow;

            //Add event handler for mouse click event of the button
            //the handler is a reference to a method with very specific signature
            button.Click += Button_Click;

            //Event handler can easily be created using code snipet
            //button.Click += <Tab>
            //Will create a new event handler and add its reference to the handlers collection

            //Add button control to Controls collection of the form to show and handle it
            Controls.Add(button);
        }

        private void Button_Click(object sender, EventArgs args)
        {
            //On pressing the button, change contents of Label called message
            message.Text = "Hello";
        }
    }
}
