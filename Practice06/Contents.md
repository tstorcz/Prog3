# Contents of Practice 06
In this lecture you see how to:
- event management and control
- WindowsForms application
- Graphical User Interface (GUI)
- GUI components - Controls

## 00 - Event handlig
This application shows step-by-step how event handling is built-up using method references as callbacks.
  
## 01 - Window from Console
Create and show first Windows Form from Console application.  
Hide console (Command Line Interface - CLI) window, to let focus on graphical interface (GUI)
  
## 02 & 03 - Windows Forms application
Design the application using UI designed, create event handler, use properties of other controls.

## 06 - Usage of ListBox control
Use a ListBox control to present a collection, create an event handler to operate on item seletion, access the selected item and its properties.