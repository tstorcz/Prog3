﻿namespace Events
{
    interface IKeyEventHandler
    {
        bool HandleKeyEvent(char eventKey);
    }
}
