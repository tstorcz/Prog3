﻿using System;

namespace Events
{
    class ContinousEventLoop : IEventLoop
    {
        private IKeyEventHandler handler;
        private int markedLoops = 10000;
        private int loopCount;
        protected bool nextIterationStep;

        public ContinousEventLoop(IKeyEventHandler handler, int markedLoops=10000)
        {
            this.handler = handler;
            this.markedLoops = markedLoops;
        }

        protected void ResetMarker()
        {
            loopCount = 0;
        }

        protected void ShowLoopMarker()
        {
            if (++loopCount > markedLoops)
            {
                loopCount = 0;
                Console.Write(".");
            }
        }

        public virtual void Run()
        {
            ResetMarker();

            nextIterationStep = true;

            while (nextIterationStep)
            {
                ShowLoopMarker();

                if (Console.KeyAvailable)
                {
                    ConsoleKeyInfo key = Console.ReadKey(false);

                    if (handler != null)
                    {
                        nextIterationStep = handler.HandleKeyEvent(char.ToLower(key.KeyChar));
                    }
                }
            }
        }
    }
}
