﻿using System;

namespace Events
{
    class InterruptedEventLoop : IEventLoop
    {
        private IKeyEventHandler handler;

        public InterruptedEventLoop(IKeyEventHandler handler)
        {
            this.handler = handler;
        }

        public void Run()
        {
            bool nextIterationStep = true;

            while (nextIterationStep)
            {
                Console.Write(".");

                ConsoleKeyInfo key = Console.ReadKey(false);

                nextIterationStep = handler.HandleKeyEvent(
                        char.ToLower(key.KeyChar)
                    );
            }
        }
    }
}
