﻿using System;

namespace Events
{
    class KeyEventArgs : EventArgs
    {
        public char Char { get; private set; }
        public KeyEventArgs(char eventChar)
        {
            Char = eventChar;
        }
    }
}
