﻿using System;
using System.Collections.Generic;

namespace _00_HF2
{
    class Controller
    {
        //Declare the storage
        LibraryGeneric myLibrary;

        public Controller()
        {
            //Create the storage
            myLibrary = new LibraryGeneric();
        }

        public void Start()
        {
            //Set prices generic for all instances
            Publication.BasePrice = 100;
            Book.AdditionalPrice = 110;
            Magazine.AdditionalPrice = 23;

            //Reguster items in storage
            AddItems();

            //Access original list through lenght and indexer
            PrintFullList();

            //List all publications in one list
            //through a copy of the original list
            ListPublications("Full list", myLibrary.GetAll());


            string titlePart = "the";   //Could be read from Console
            ListBooks(titlePart);


            titlePart = "digest";       //Could be read from Console
            ListMagazines(titlePart);

            //How about extending the model with a brand new class, what should be modified???
            HandleRecords();
        }

        private void AddItems()
        {
            myLibrary.Add(
                new Book("Lord of the Rings", "Harper Collins Publishers", "J.R.R.Tolkien", 1664)
            );

            myLibrary.Add(
                new Book("Dune", "Chilton Books", "Frank Herbert", 412)
            );

            myLibrary.Add(
                new Book("Harry Potter and the Philosopher's Stone", "Bloomsbury", "J. K. Rowling", 223)
            );

            myLibrary.Add(
                new Magazine("Reader's digest", "Trusted Media Brands", 2002, 5)
            );

            myLibrary.Add(
                new Magazine("Reader's digest", "Trusted Media Brands", 2002, 8)
            );

            myLibrary.Add(
                new Magazine("National Geographic", "National Geographic Partners", 1998, 3)
            );

            myLibrary.Add(
                new Magazine("National Geographic", "National Geographic Partners", 2001, 3)
            );
        }

        public void PrintFullList()
        {
            Console.WriteLine("======== Print full list");
            for(int i=0; i < myLibrary.Count; i++)
            {
                Console.WriteLine(myLibrary[i]);
            }
        }

        //Print received list of publications
        //with title
        //and with total price of a given rent period
        private void ListPublications(string title, List<Publication> pubList)
        {
            Console.WriteLine("---=== {0}", title);
            Console.Write("Planned rental period length [day]: ");
            int days = int.Parse(Console.ReadLine());
            foreach (var p in pubList)
            {
                Console.WriteLine(
                    string.Format(
                        "{0} for {1} days: {2}",
                        p.ToString(), days, p.RentalPrice(days)
                        )
                    );
            }
        }

        private void ListBooks(string titlePart)
        {
            //List books which title contain 'the'
            //Use a lambda expression in as a filter criteria
            //utilize that lambda can access local variables of container
            ListPublications(
                string.Format("Books with '{0}' in title", titlePart),
                myLibrary.Find((p) => p.Title.Contains(titlePart) && p is Book)
            );
        }

        private void ListMagazines(string titlePart)
        {
            //List magazines which title contain 'digest'
            //Use a lambda expression in as a filter criteria
            //utilize that lambda can access local variables of container
            ListPublications(
                string.Format("Magazines with '{0}' in title", titlePart),
                myLibrary.Find((p) => p.Title.Contains(titlePart) && p is Magazine)
            );

            //Using generic method with type parameter to return explicitly typed array
            TypedListWithGenericMethod(titlePart);
        }

        private void TypedListWithGenericMethod(string titlePart)
        {
            //Using generic method with type parameter to return explicitly typed array
            //type check has been removed from filter lambda expression,
            //because it is done by application of the type parameter in generic method
            List<Magazine> magazinesFound =
                myLibrary.Find<Magazine>((p) => p.Title.Contains(titlePart));

            //ListPublications method can not be used, because type parameter of 
            //a generic list can not be explicitly casted, therefore list is printed in-line
            Console.WriteLine("---=== Explicit magazines found");
            Console.Write("Planned rental period length [day]: ");
            int days = int.Parse(Console.ReadLine());
            foreach (var p in magazinesFound)
            {
                Console.WriteLine(
                    string.Format(
                        "{0} for {1} days: {2}",
                        p.ToString(), days, p.RentalPrice(days)
                        )
                    );
            }
        }

        //Handling brand new type
        //Due to correct encapsulation, does NOT require to modify other existing classes
        private void HandleRecords()
        {
            //Inherit a new class Record from publications

            //Set generic price multiplier applied for ALL records
            Record.AdditionalPrice = 222;

            //Add items to the collection
            #region Add Record items to the collection

            myLibrary.Add(
                new Record("News of the World", "Sarm West and Wessex Sound", "Queen", 11, 40)
            );

            myLibrary.Add(
                new Record("Can't Take Me Home", "Unique Recording Studios", "Pink", 13, 55)
            );

            myLibrary.Add(
                new Record("The Slim Shady LP", "Studio 8", "Eminem", 20, 60)
            );

            #endregion

            string titlePart = "the";       //Could be read from Console

            //List records which title contain 'digest'
            //Use a lambda expression in as a filter criteria
            //utilize that lambda can access local variables of container
            ListPublications(
                string.Format("Records with '{0}' in title", titlePart),
                myLibrary.Find((p) => p.Title.ToLower().Contains(titlePart) && p is Record)
            );

            //=============================================================

            //Find records with generic method and type parameter
            //no need to chande Library class
            List<Record> magazinesFound =
                myLibrary.Find<Record>((p) => p.Title.Contains(titlePart));
        }
    }
}
