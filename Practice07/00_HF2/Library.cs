﻿using System.Collections.Generic;

namespace _00_HF2
{
    class Library
    {
        //Protected data member, to make accesssible in descendants
        //Collection is not published
        protected List<Publication> publications;

        //Enumerator could be created to enable
        //accessing collection items directly and to provie enumeration
        public int Count { get { return publications.Count; } }
        public Publication this[int index] {
            get { return publications[index]; }
        }

        public Library()
        {
            publications = new List<Publication>();
        }

        //Endpoint to add new item to the collection
        public void Add(Publication newPublication)
        {
            publications.Add(newPublication);
        }

        #region Find methods

        //!!!!!!!!!!!!!!!!!!!! NOT DRY - Don't Repeat Yourself

        public List<Book> FindBooks(string title)
        {
            List<Book> found = new List<Book>();

            foreach (var p in publications)
            {
                if (p is Book && p.Title.ToLower().Contains(title.ToLower()))
                {
                    found.Add(p as Book);
                }
            }

            return found;
        }

        public List<Magazine> FindMagazines(string title)
        {
            List<Magazine> found = new List<Magazine>();

            foreach (var p in publications)
            {
                if (p is Magazine && p.Title.ToLower().Contains(title.ToLower()))
                {
                    found.Add(p as Magazine);
                }
            }

            return found;
        }

        public List<Publication> FindPublications(string title)
        {
            List<Publication> found = new List<Publication>();

            foreach (var p in publications)
            {
                if (p.Title.ToLower().Contains(title.ToLower()))
                {
                    found.Add(p);
                }
            }

            return found;
        }

        //!!!!!!!!!!!!!!!!!!!!

        #endregion
    }
}
