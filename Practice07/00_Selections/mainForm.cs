﻿using System.Windows.Forms;

namespace _00_Selections
{
    public partial class mainForm : Form
    {
        public mainForm()
        {
            InitializeComponent();
        }

        private void ToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            MessageBox.Show("Selected: " + ((ToolStripMenuItem)sender).Text);
        }
    }
}
