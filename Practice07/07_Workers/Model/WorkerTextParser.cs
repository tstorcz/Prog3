﻿using System;
using System.Collections.Generic;

namespace Workers
{
    //The only responsibility of this class is to parse a worker list from a multiline text
    //This is the only class depending on text format
    //
    //Processed structure:
    //      Firstname1,Lastname1,job1,salary1,{ability1:level1; ability2:level2}
    //
    //It implements the ITextParser interface to make itself injectable
    class WorkerTextParser : ITextParser<Worker>
    {
        public List<Worker> ParseListFromMultilineText(string multilineText)
        {
            List<Worker> workers = new List<Worker>();

            //Split on new-line characters
            string[] lines = multilineText.Split(
                Environment.NewLine.ToCharArray(),
                StringSplitOptions.RemoveEmptyEntries
            );

            workers.Clear();

            //Add worker instances
            foreach (string line in lines)
            {
                string[] props = line.Trim().Split(',');

                workers.Add(
                    new Worker(
                        props[1].Trim(),            //last name
                        props[0].Trim(),            //first name
                        props[2].Trim(),            //job
                        int.Parse(props[3].Trim()), //salary
                        props[4].Trim()             //abilities
                    )
                );
            }

            return workers;
        }
    }
}
