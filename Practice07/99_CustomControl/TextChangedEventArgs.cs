﻿namespace _99_CustomControl
{
    public class TextChangedEventArgs : EventArgs
    {
        public string Text { get; private set; }

        public TextChangedEventArgs(string newText)
        {
            Text = newText;
        }
    }
}
