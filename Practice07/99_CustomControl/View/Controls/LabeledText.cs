﻿using System.ComponentModel;

namespace _99_CustomControl
{
    public partial class LabeledText : UserControl
    {
        public delegate void TextChangedEventHandler(object sender, TextChangedEventArgs args);

        public event TextChangedEventHandler? TextChangedEvent;

        [Category("Custom")]
        [Browsable(true)]
        [Description("Sets the label of text")]
        public string Label
        {
            get { return label.Text; }
            set { label.Text = value; }
        }

        [Category("Custom")]
        [Browsable(true)]
        [Description("Sets the text")]
        public string EnteredText {
            get { return text.Text; }
            set { text.Text = value; }
        }

        public LabeledText()
        {
            InitializeComponent();
        }

        private void text_TextChanged(object sender, EventArgs e)
        {
            TextChangedEvent?.Invoke(this, new TextChangedEventArgs(text.Text));
        }
    }
}
