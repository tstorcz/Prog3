﻿namespace _99_CustomControl
{
    partial class mainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labeledText1 = new _99_CustomControl.LabeledText();
            this.labeledText2 = new _99_CustomControl.LabeledText();
            this.labeledText3 = new _99_CustomControl.LabeledText();
            this.controlContainer = new System.Windows.Forms.FlowLayoutPanel();
            this.addControls = new System.Windows.Forms.Button();
            this.newControlCount = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.newControlCount)).BeginInit();
            this.SuspendLayout();
            // 
            // labeledText1
            // 
            this.labeledText1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.labeledText1.EnteredText = "MyText 1";
            this.labeledText1.Label = "Text 1";
            this.labeledText1.Location = new System.Drawing.Point(15, 19);
            this.labeledText1.Margin = new System.Windows.Forms.Padding(5, 8, 5, 8);
            this.labeledText1.Name = "labeledText1";
            this.labeledText1.Size = new System.Drawing.Size(281, 88);
            this.labeledText1.TabIndex = 2;
            this.labeledText1.TextChangedEvent += new _99_CustomControl.LabeledText.TextChangedEventHandler(this.labeledText1_TextChangedEvent);
            // 
            // labeledText2
            // 
            this.labeledText2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.labeledText2.EnteredText = "MyText 2";
            this.labeledText2.Label = "Text 2";
            this.labeledText2.Location = new System.Drawing.Point(304, 19);
            this.labeledText2.Margin = new System.Windows.Forms.Padding(5, 8, 5, 8);
            this.labeledText2.Name = "labeledText2";
            this.labeledText2.Size = new System.Drawing.Size(281, 88);
            this.labeledText2.TabIndex = 3;
            // 
            // labeledText3
            // 
            this.labeledText3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.labeledText3.EnteredText = "MyText 3";
            this.labeledText3.Label = "Text 3";
            this.labeledText3.Location = new System.Drawing.Point(592, 19);
            this.labeledText3.Margin = new System.Windows.Forms.Padding(5, 8, 5, 8);
            this.labeledText3.Name = "labeledText3";
            this.labeledText3.Size = new System.Drawing.Size(281, 88);
            this.labeledText3.TabIndex = 4;
            // 
            // controlContainer
            // 
            this.controlContainer.AutoScroll = true;
            this.controlContainer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.controlContainer.Location = new System.Drawing.Point(15, 196);
            this.controlContainer.Name = "controlContainer";
            this.controlContainer.Size = new System.Drawing.Size(873, 587);
            this.controlContainer.TabIndex = 5;
            // 
            // addControls
            // 
            this.addControls.Location = new System.Drawing.Point(421, 118);
            this.addControls.Name = "addControls";
            this.addControls.Size = new System.Drawing.Size(189, 57);
            this.addControls.TabIndex = 6;
            this.addControls.Text = "Add controls";
            this.addControls.UseVisualStyleBackColor = true;
            this.addControls.Click += new System.EventHandler(this.addControls_Click);
            // 
            // newControlCount
            // 
            this.newControlCount.Location = new System.Drawing.Point(281, 132);
            this.newControlCount.Maximum = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.newControlCount.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.newControlCount.Name = "newControlCount";
            this.newControlCount.Size = new System.Drawing.Size(93, 31);
            this.newControlCount.TabIndex = 7;
            this.newControlCount.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // mainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(919, 795);
            this.Controls.Add(this.newControlCount);
            this.Controls.Add(this.addControls);
            this.Controls.Add(this.controlContainer);
            this.Controls.Add(this.labeledText3);
            this.Controls.Add(this.labeledText2);
            this.Controls.Add(this.labeledText1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "mainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Main form";
            ((System.ComponentModel.ISupportInitialize)(this.newControlCount)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private LabeledText labeledText1;
        private LabeledText labeledText2;
        private LabeledText labeledText3;
        private FlowLayoutPanel controlContainer;
        private Button addControls;
        private NumericUpDown newControlCount;
    }
}

