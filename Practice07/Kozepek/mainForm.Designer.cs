﻿namespace Kozepek
{
    partial class mainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Compute = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.X = new System.Windows.Forms.TextBox();
            this.Y = new System.Windows.Forms.TextBox();
            this.Avg1 = new System.Windows.Forms.TextBox();
            this.Avg2 = new System.Windows.Forms.TextBox();
            this.Avg3 = new System.Windows.Forms.TextBox();
            this.Clear = new System.Windows.Forms.Button();
            this.Finish = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // Compute
            // 
            this.Compute.Enabled = false;
            this.Compute.Location = new System.Drawing.Point(30, 255);
            this.Compute.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Compute.Name = "Compute";
            this.Compute.Size = new System.Drawing.Size(84, 41);
            this.Compute.TabIndex = 0;
            this.Compute.Text = "Számol";
            this.Compute.UseVisualStyleBackColor = true;
            this.Compute.Click += new System.EventHandler(this.Compute_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(45, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(20, 20);
            this.label1.TabIndex = 1;
            this.label1.Text = "X";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(164, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(20, 20);
            this.label2.TabIndex = 2;
            this.label2.Text = "Y";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(15, 115);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(76, 20);
            this.label3.TabIndex = 3;
            this.label3.Text = "Számtani";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(15, 150);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(62, 20);
            this.label4.TabIndex = 4;
            this.label4.Text = "Mértani";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(15, 185);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(94, 20);
            this.label5.TabIndex = 5;
            this.label5.Text = "Harmonikus";
            // 
            // X
            // 
            this.X.Location = new System.Drawing.Point(48, 41);
            this.X.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.X.Name = "X";
            this.X.Size = new System.Drawing.Size(112, 26);
            this.X.TabIndex = 6;
            this.X.TextChanged += new System.EventHandler(this.Value_TextChanged);
            // 
            // Y
            // 
            this.Y.Location = new System.Drawing.Point(168, 41);
            this.Y.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Y.Name = "Y";
            this.Y.Size = new System.Drawing.Size(112, 26);
            this.Y.TabIndex = 7;
            this.Y.TextChanged += new System.EventHandler(this.Value_TextChanged);
            // 
            // Avg1
            // 
            this.Avg1.Location = new System.Drawing.Point(115, 111);
            this.Avg1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Avg1.Name = "Avg1";
            this.Avg1.ReadOnly = true;
            this.Avg1.Size = new System.Drawing.Size(112, 26);
            this.Avg1.TabIndex = 8;
            // 
            // Avg2
            // 
            this.Avg2.Location = new System.Drawing.Point(115, 146);
            this.Avg2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Avg2.Name = "Avg2";
            this.Avg2.ReadOnly = true;
            this.Avg2.Size = new System.Drawing.Size(112, 26);
            this.Avg2.TabIndex = 9;
            // 
            // Avg3
            // 
            this.Avg3.Location = new System.Drawing.Point(115, 181);
            this.Avg3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Avg3.Name = "Avg3";
            this.Avg3.ReadOnly = true;
            this.Avg3.Size = new System.Drawing.Size(112, 26);
            this.Avg3.TabIndex = 10;
            // 
            // Clear
            // 
            this.Clear.Location = new System.Drawing.Point(115, 255);
            this.Clear.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Clear.Name = "Clear";
            this.Clear.Size = new System.Drawing.Size(84, 41);
            this.Clear.TabIndex = 11;
            this.Clear.Text = "Törlés";
            this.Clear.UseVisualStyleBackColor = true;
            this.Clear.Click += new System.EventHandler(this.Clear_Click);
            // 
            // Finish
            // 
            this.Finish.Location = new System.Drawing.Point(199, 255);
            this.Finish.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Finish.Name = "Finish";
            this.Finish.Size = new System.Drawing.Size(84, 41);
            this.Finish.TabIndex = 12;
            this.Finish.Text = "Vége";
            this.Finish.UseVisualStyleBackColor = true;
            this.Finish.Click += new System.EventHandler(this.Finish_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(308, 311);
            this.Controls.Add(this.Finish);
            this.Controls.Add(this.Clear);
            this.Controls.Add(this.Avg3);
            this.Controls.Add(this.Avg2);
            this.Controls.Add(this.Avg1);
            this.Controls.Add(this.Y);
            this.Controls.Add(this.X);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Compute);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Közepek";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Compute;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox X;
        private System.Windows.Forms.TextBox Y;
        private System.Windows.Forms.TextBox Avg1;
        private System.Windows.Forms.TextBox Avg2;
        private System.Windows.Forms.TextBox Avg3;
        private System.Windows.Forms.Button Clear;
        private System.Windows.Forms.Button Finish;
    }
}

