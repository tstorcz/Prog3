﻿using System;
using System.Windows.Forms;

namespace SzovegTordelo
{
    public partial class mainForm : Form
    {
        public mainForm()
        {
            InitializeComponent();
        }

        private void Brake_Click(object sender, EventArgs e)
        {
            BrakeIntoWords();
        }

        private void BrakeIntoWords()
        {
            String[] words = Sentence.Text.Split(' ');
            foreach (String word in words)
            {
                Words.Text += word + Environment.NewLine;
            }
        }

        private void Sentence_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                BrakeIntoWords();
            }
        }
    }
}
