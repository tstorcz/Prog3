﻿using System;

namespace xx_CustomControl
{
    public class TextChangedEventArgs : EventArgs
    {
        public string Text { get; private set; }

        public TextChangedEventArgs(string newText)
        {
            Text = newText;
        }
    }
}
