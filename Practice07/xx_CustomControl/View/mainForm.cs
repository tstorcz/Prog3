﻿
using System;
using System.Windows.Forms;

namespace xx_CustomControl
{
    public partial class mainForm : Form
    {
        public mainForm()
        {
            InitializeComponent();
        }

        private void addControls_Click(object sender, EventArgs e)
        {
            LabeledText newControl;
            controlContainer.Controls.Clear();
            for (int i = 0; i < newControlCount.Value; i++)
            {
                newControl = new LabeledText();
                newControl.Text = i.ToString();
                controlContainer.Controls.Add(newControl);
            }
        }

        private void labeledText1_TextChangedEvent(object sender, TextChangedEventArgs args)
        {
            labeledText2.EnteredText = args.Text;
        }
    }
}
