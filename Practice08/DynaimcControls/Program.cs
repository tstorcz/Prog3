﻿using System;
using System.Windows.Forms;

namespace DynaimcControls
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            //Show splash screen (it will close itself)
            new splashForm().ShowDialog();

            //Show main form after splash closed automatically
            Application.Run(new mainForm());
        }
    }
}
