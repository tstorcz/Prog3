﻿using System.ComponentModel;
using System.Windows.Forms;

namespace DynaimcControls
{
    public partial class LabeledText : UserControl
    {
        [Category("Custom")]
        [Browsable(true)]
        [Description("Sets the label of text")]
        public string Label
        {
            get { return label.Text; }
            set { label.Text = value; }
        }

        [Category("Custom")]
        [Browsable(true)]
        [Description("Sets the text")]
        public string EnteredText {
            get { return text.Text; }
            set { text.Text = value; }
        }

        public LabeledText()
        {
            InitializeComponent();
        }
    }
}
