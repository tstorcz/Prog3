﻿using System.Collections.Generic;

namespace PizzaOrder
{
    interface IPizzaParser
    {
        List<Pizza> Parse(string multiLineText);
    }
}
