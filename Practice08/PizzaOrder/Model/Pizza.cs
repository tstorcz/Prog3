﻿namespace PizzaOrder
{
    class Pizza
    {
        public string Nev { get; private set; }
        public int ArKicsi { get; set; }
        public int ArNagy { get; set; }

        public Pizza(string nev, int arKicsi, int arNagy)
        {
            this.Nev = nev;
            this.ArKicsi = arKicsi;
            this.ArNagy = arNagy;
        }
    }
}
