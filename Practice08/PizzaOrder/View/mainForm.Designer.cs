﻿namespace PizzaOrder
{
    partial class mainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblNagy = new System.Windows.Forms.Label();
            this.lblKicsi = new System.Windows.Forms.Label();
            this.adatBevitel = new System.Windows.Forms.Button();
            this.fizetendo = new System.Windows.Forms.TextBox();
            this.fizetendoLabel = new System.Windows.Forms.Label();
            this.torol = new System.Windows.Forms.Button();
            this.szamol = new System.Windows.Forms.Button();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.dynamicContentContainer = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.BackImage = new System.Windows.Forms.PictureBox();
            this.CloseWindow = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.BackImage)).BeginInit();
            this.SuspendLayout();
            // 
            // lblNagy
            // 
            this.lblNagy.AutoSize = true;
            this.lblNagy.Location = new System.Drawing.Point(418, 77);
            this.lblNagy.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblNagy.Name = "lblNagy";
            this.lblNagy.Size = new System.Drawing.Size(55, 17);
            this.lblNagy.TabIndex = 38;
            this.lblNagy.Text = "lblNagy";
            // 
            // lblKicsi
            // 
            this.lblKicsi.AutoSize = true;
            this.lblKicsi.Location = new System.Drawing.Point(293, 77);
            this.lblKicsi.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblKicsi.Name = "lblKicsi";
            this.lblKicsi.Size = new System.Drawing.Size(51, 17);
            this.lblKicsi.TabIndex = 37;
            this.lblKicsi.Text = "lblKicsi";
            // 
            // adatBevitel
            // 
            this.adatBevitel.Location = new System.Drawing.Point(309, 355);
            this.adatBevitel.Margin = new System.Windows.Forms.Padding(4);
            this.adatBevitel.Name = "adatBevitel";
            this.adatBevitel.Size = new System.Drawing.Size(211, 28);
            this.adatBevitel.TabIndex = 36;
            this.adatBevitel.Text = "Adatbevitel";
            this.adatBevitel.UseVisualStyleBackColor = true;
            this.adatBevitel.Click += new System.EventHandler(this.AdatBevitel_Click);
            // 
            // fizetendo
            // 
            this.fizetendo.Location = new System.Drawing.Point(369, 298);
            this.fizetendo.Margin = new System.Windows.Forms.Padding(4);
            this.fizetendo.Name = "fizetendo";
            this.fizetendo.ReadOnly = true;
            this.fizetendo.Size = new System.Drawing.Size(132, 22);
            this.fizetendo.TabIndex = 34;
            this.fizetendo.TabStop = false;
            this.fizetendo.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // fizetendoLabel
            // 
            this.fizetendoLabel.AutoSize = true;
            this.fizetendoLabel.Font = new System.Drawing.Font("Garamond", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.fizetendoLabel.ForeColor = System.Drawing.Color.Maroon;
            this.fizetendoLabel.Location = new System.Drawing.Point(232, 298);
            this.fizetendoLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.fizetendoLabel.Name = "fizetendoLabel";
            this.fizetendoLabel.Size = new System.Drawing.Size(98, 22);
            this.fizetendoLabel.TabIndex = 33;
            this.fizetendoLabel.Text = "Fizetendő:";
            // 
            // torol
            // 
            this.torol.Location = new System.Drawing.Point(70, 355);
            this.torol.Margin = new System.Windows.Forms.Padding(4);
            this.torol.Name = "torol";
            this.torol.Size = new System.Drawing.Size(107, 31);
            this.torol.TabIndex = 32;
            this.torol.Text = "Töröl";
            this.torol.UseVisualStyleBackColor = true;
            this.torol.Click += new System.EventHandler(this.Torol_Click);
            // 
            // szamol
            // 
            this.szamol.Location = new System.Drawing.Point(70, 294);
            this.szamol.Margin = new System.Windows.Forms.Padding(4);
            this.szamol.Name = "szamol";
            this.szamol.Size = new System.Drawing.Size(107, 31);
            this.szamol.TabIndex = 31;
            this.szamol.Text = "Számol";
            this.szamol.UseVisualStyleBackColor = true;
            this.szamol.Click += new System.EventHandler(this.Szamol_Click);
            // 
            // dynamicContentContainer
            // 
            this.dynamicContentContainer.AutoScroll = true;
            this.dynamicContentContainer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.dynamicContentContainer.Location = new System.Drawing.Point(12, 110);
            this.dynamicContentContainer.Name = "dynamicContentContainer";
            this.dynamicContentContainer.Size = new System.Drawing.Size(800, 166);
            this.dynamicContentContainer.TabIndex = 39;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label1.Location = new System.Drawing.Point(9, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(804, 50);
            this.label1.TabIndex = 40;
            this.label1.Text = "Pizza rendelés";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // BackImage
            // 
            this.BackImage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.BackImage.Image = global::PizzaOrder.Properties.Resources.pizza;
            this.BackImage.Location = new System.Drawing.Point(0, 0);
            this.BackImage.Name = "BackImage";
            this.BackImage.Size = new System.Drawing.Size(825, 410);
            this.BackImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.BackImage.TabIndex = 41;
            this.BackImage.TabStop = false;
            // 
            // CloseWindow
            // 
            this.CloseWindow.Location = new System.Drawing.Point(737, 355);
            this.CloseWindow.Name = "CloseWindow";
            this.CloseWindow.Size = new System.Drawing.Size(75, 35);
            this.CloseWindow.TabIndex = 42;
            this.CloseWindow.Text = "Kilépés";
            this.CloseWindow.UseVisualStyleBackColor = true;
            this.CloseWindow.Click += new System.EventHandler(this.CloseWindow_Click);
            // 
            // mainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(825, 410);
            this.Controls.Add(this.CloseWindow);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblNagy);
            this.Controls.Add(this.lblKicsi);
            this.Controls.Add(this.adatBevitel);
            this.Controls.Add(this.fizetendo);
            this.Controls.Add(this.fizetendoLabel);
            this.Controls.Add(this.torol);
            this.Controls.Add(this.szamol);
            this.Controls.Add(this.dynamicContentContainer);
            this.Controls.Add(this.BackImage);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "mainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Pizza rendelés";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.mainForm_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.BackImage)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblNagy;
        private System.Windows.Forms.Label lblKicsi;
        private System.Windows.Forms.Button adatBevitel;
        private System.Windows.Forms.TextBox fizetendo;
        private System.Windows.Forms.Label fizetendoLabel;
        private System.Windows.Forms.Button torol;
        private System.Windows.Forms.Button szamol;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.Panel dynamicContentContainer;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox BackImage;
        private System.Windows.Forms.Button CloseWindow;
    }
}

