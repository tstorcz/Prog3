﻿namespace Retoxi
{
    partial class HelpForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(HelpForm));
            this.helpText = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.close = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.helpYes = new System.Windows.Forms.RadioButton();
            this.helpNo = new System.Windows.Forms.RadioButton();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // helpText
            // 
            this.helpText.Location = new System.Drawing.Point(12, 55);
            this.helpText.Multiline = true;
            this.helpText.Name = "helpText";
            this.helpText.ReadOnly = true;
            this.helpText.Size = new System.Drawing.Size(776, 345);
            this.helpText.TabIndex = 0;
            this.helpText.TabStop = false;
            this.helpText.Text = resources.GetString("helpText.Text");
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(-69, 279);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // close
            // 
            this.close.Location = new System.Drawing.Point(680, 406);
            this.close.Name = "close";
            this.close.Size = new System.Drawing.Size(108, 32);
            this.close.TabIndex = 2;
            this.close.Text = "Bezár";
            this.close.UseVisualStyleBackColor = true;
            this.close.Click += new System.EventHandler(this.close_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(17, 414);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(111, 17);
            this.label1.TabIndex = 3;
            this.label1.Text = "Segített a súgó?";
            // 
            // helpYes
            // 
            this.helpYes.AutoSize = true;
            this.helpYes.Location = new System.Drawing.Point(159, 412);
            this.helpYes.Name = "helpYes";
            this.helpYes.Size = new System.Drawing.Size(56, 21);
            this.helpYes.TabIndex = 4;
            this.helpYes.TabStop = true;
            this.helpYes.Text = "Igen";
            this.helpYes.UseVisualStyleBackColor = true;
            // 
            // helpNo
            // 
            this.helpNo.AutoSize = true;
            this.helpNo.Location = new System.Drawing.Point(245, 412);
            this.helpNo.Name = "helpNo";
            this.helpNo.Size = new System.Drawing.Size(58, 21);
            this.helpNo.TabIndex = 5;
            this.helpNo.TabStop = true;
            this.helpNo.Text = "Nem";
            this.helpNo.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label2.Location = new System.Drawing.Point(12, 14);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(776, 38);
            this.label2.TabIndex = 6;
            this.label2.Text = "Kérem, a \"Bezár\" gombbal zárja be az ablakot!";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // HelpForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.helpNo);
            this.Controls.Add(this.helpYes);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.close);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.helpText);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "HelpForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "HelpForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox helpText;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button close;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton helpYes;
        private System.Windows.Forms.RadioButton helpNo;
        private System.Windows.Forms.Label label2;
    }
}