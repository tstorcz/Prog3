﻿using System.Windows.Forms;

namespace Retoxi
{
    public partial class HelpForm : Form
    {
        public enum HelpUsefulness
        {
            Yes,
            No,
            NotSelected
        }

        public HelpUsefulness DidHelp {
            get
            {
                return helpYes.Checked ? HelpUsefulness.Yes : (helpNo.Checked ? HelpUsefulness.No : HelpUsefulness.NotSelected);
            }
        }

        public HelpForm()
        {
            InitializeComponent();
        }

        private void close_Click(object sender, System.EventArgs e)
        {
            DialogResult = DialogResult.OK;
            Close();
        }
    }
}
