﻿namespace Retoxi.Model.Control
{
    internal interface IDrinkControl
    {
        Drink Model { get; }

        bool Checked { get; }

        string Pieces { get; }

        bool HasError { get; set; }

        void Reset();
    }
}
