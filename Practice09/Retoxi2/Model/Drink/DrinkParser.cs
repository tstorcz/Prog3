﻿using Retoxi.Model;

namespace Retoxi
{
    class DrinkParser : IParser<Drink>
    {
        public Drink Parse(string[] input)
        {
            return new Drink(
                            input[0],
                            int.Parse(input[1])
                        );
        }

    }
}
