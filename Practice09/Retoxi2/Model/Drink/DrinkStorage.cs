﻿using System.Collections;
using System.Collections.Generic;

namespace Retoxi
{
    public class DrinkStorage : IEnumerable<Drink>
    {
        private List<Drink> drinks;

        public int Count { get { return drinks.Count; } }

        public Drink this[int index]
        {
            get { return drinks[index]; }
        }

        public DrinkStorage()
        {
            drinks = new List<Drink>();
        }

        public void Clear()
        {
            drinks.Clear();
        }

        public void Add(Drink newItem)
        {
            drinks.Add(newItem);
        }

        public void AddRange(IEnumerable<Drink> newItems)
        {
            drinks.AddRange(newItems);
        }

        public IEnumerator<Drink> GetEnumerator()
        {
            return new DrinkStorageEnumerator(this);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
