﻿namespace Retoxi.Model
{
    public interface IParser<T>
    {
        T Parse(string[] input);
    }
}
