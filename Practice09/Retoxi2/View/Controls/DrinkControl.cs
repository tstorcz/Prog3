﻿using Retoxi.Model.Control;
using System.Drawing;
using System.Windows.Forms;

namespace Retoxi.View.Controls
{
    public partial class DrinkControl : UserControl, IDrinkControl
    {
        public static string PortionText { get; set; }

        private bool hasError;

        public Drink Model { get; private set; }

        public bool Checked { get { return order.Checked; } }

        public string Pieces { get { return count.Text; } }

        public bool HasError
        {
            get { return hasError; }
            set
            {
                hasError = value;
                count.BackColor = hasError ? Color.Salmon : Color.White;
            }
        }

        public DrinkControl(Drink drink)
        {
            Model = drink;

            InitializeComponent();

            order.Text = drink.Name;
            portion.Text = PortionText;
        }

        public int GetOrderPrice()
        {
            return int.Parse(count.Text) * Model.UnitPrice;
        }

        public void Reset()
        {
            order.Checked = false;
            count.Clear();
            HasError = false;
        }
    }
}
