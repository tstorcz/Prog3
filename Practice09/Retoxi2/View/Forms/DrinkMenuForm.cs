﻿using Retoxi.Model.Control;
using Retoxi.View.Controls;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace Retoxi
{
    public partial class DrinkMenuForm : Form
    {
        private DrinkStorage storage;
        private List<IDrinkControl> drinkControls = new List<IDrinkControl>();

        public DrinkMenuForm(DrinkStorage storage)
        {
            InitializeComponent();
            this.storage = storage;
        }

        private const int maxPortion = 999;

        private void ItallapForm_Load(object sender, EventArgs e)
        {
            //CreateDynamicFormContentManually();
            CreateDynamicFormContentWithUserControl();
        }

        #region Manual control management

        private void CreateDynamicFormContentManually()
        {
            Panel container = new Panel();
            container.Location = new Point(14, 56);
            container.Size = new Size(458, 324);
            Controls.Add(container);

            for (int i = 0; i < storage.Count; i++)
            {
                CreateControlsOfItem(storage[i], i, container);
            }
        }

        private const int left = 10, top = 10;
        private const int chkXSize = 250, chkYSize = 21;
        private const int chkYDistance = 40;
        private const int txtXSize = 30, txtYSize = 17;
        private const int lblXSize = 46;

        private const int xSpace = 5;

        private void CreateControlsOfItem(Drink item, int index, Control container)
        {
            CheckBox name;
            TextBox portion;
            Label portionLabel;

            name = new CheckBox();
            name.Text = DrinkPrinter.PrintToPriceList(item);
            name.Location = new Point(left, top + index * chkYDistance);
            name.Size = new Size(chkXSize, chkYSize);

            portion = new TextBox();
            portion.Location = new Point(
                left + name.Size.Width + xSpace,
                top + index * chkYDistance
            );
            portion.MaxLength = 3;
            portion.Size = new Size(txtXSize, txtYSize);

            portionLabel = new Label();
            portionLabel.Text = "portion";
            portionLabel.Location = new Point(
                portion.Location.X + portion.Size.Width + xSpace,
                top + index * chkYDistance
            );
            portionLabel.Size = new Size(lblXSize, txtYSize);

            container.Controls.Add(name);
            container.Controls.Add(portion);
            container.Controls.Add(portionLabel);

            drinkControls.Add(new DrinkMenuControl(item, name, portion));
        }

        #endregion

        #region UserControl management

        private void CreateDynamicFormContentWithUserControl()
        {
            FlowLayoutPanel container = new FlowLayoutPanel();
            container.FlowDirection = FlowDirection.LeftToRight;
            container.AutoScroll = true;
            container.Location = new Point(14, 56);
            container.Size = new Size(458, 324);
            Controls.Add(container);

            DrinkControl.PortionText = "portion";

            for (int i = 0; i < storage.Count; i++)
            {
                drinkControls.Add(new DrinkControl(storage[i]));
                container.Controls.Add((Control)drinkControls[i]);
            }
        }

        #endregion

        private void Order_Click(object sender, EventArgs e)
        {
            bool hasError = false;
            foreach(var control in drinkControls)
            {
                if (control.Checked)
                {
                    int db = 0;
                    bool isNumber = int.TryParse(control.Pieces, out db);

                    if (!isNumber || db < 1 || db > maxPortion)
                    {
                        control.HasError = true;
                        hasError = true;
                    }
                    else
                    {
                        control.Model.UnpayedOrder += db;
                        control.Reset();
                    }
                }
                else
                {
                    control.Reset();
                }
            }

            if (hasError)
            {
                MessageBox.Show(
                    "Data marked with red background are invalid!",
                    "Attention!"
                );
            }
        }

        private void billToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OrdersForm ordersForm = new OrdersForm(storage);
            ordersForm.ShowDialog();
        }

        private void payToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach(Drink ital in storage)
            {
                ital.PayedOrder += ital.UnpayedOrder;
                ital.UnpayedOrder = 0;
            }

            MessageBox.Show("The invoice has been payed!");
        }
    }
}
