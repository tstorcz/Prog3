﻿using System;
using System.Text;
using System.Windows.Forms;

namespace Retoxi
{
    public partial class OrdersForm : Form
    {
        private DrinkStorage storage;

        public OrdersForm(DrinkStorage storage)
        {
            InitializeComponent();

            this.storage = storage;
        }

        private void OrdersForm_Load(object sender, EventArgs e)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("Order summary:");

            int total = 0;
            foreach (Drink drink in storage)
            {
                if(drink.OrderPrice > 0)
                {
                    sb.AppendLine(DrinkPrinter.PrintToInvoice(drink));
                }
                total += drink.OrderPrice;
            }

            sb.AppendLine(String.Format("Total: {0} Ft", total.ToString()));

            Orders.Text = sb.ToString();
        }
    }
}
