﻿using GitLogParser.Model;
using System;
using System.Windows.Forms;

namespace GitLogParser
{
    public partial class MainForm : Form
    {
        private GitData gitData = new GitData();

        public MainForm()
        {
            InitializeComponent();

            LoadLogFile(@"..\..\data\git.txt");

            MessageBox.Show(
                string.Format("{0} file loaded automatically", @"..\..\data\git.txt"), 
                "Information", 
                MessageBoxButtons.OK, 
                MessageBoxIcon.Information
            );
        }

        private void LoadLogFile(string fileName)
        {
            //fileContents.Text = System.IO.File.ReadAllText(fileName);

            string[] lines = System.IO.File.ReadAllLines(fileName);

            fileContents.Text = String.Empty;
            foreach (string line in lines)
            {
                fileContents.Text += String.Format("{0}{1}", line, Environment.NewLine);
            }

            gitData.Clear();
            Parser.ParseAll(lines, gitData);

            log.Items.Clear();
            for (int i = 0; i<gitData.Count; i++)
            {
                log.Items.Add(gitData[i]);
            }
        }

        private void parse_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            if(ofd.ShowDialog() == DialogResult.OK)
            {
                LoadLogFile(ofd.FileName);
            }
        }
    }
}
