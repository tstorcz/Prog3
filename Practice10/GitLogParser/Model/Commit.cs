﻿namespace GitLogParser
{
    public class Commit
    {
        public string Id { get; private set; }
        public string Author { get; private set; }
        public string Date { get; private set; }
        public string Description { get; private set; }
        public int FileCount { get; private set; }
        public int Insert { get; private set; }
        public int Delete { get; private set; }

        public Commit(string id, string author, string date, string description, int fileCount, int insert, int delete)
        {
            Id = id;
            Author = author;
            Date = date;
            Description = description;
            FileCount = fileCount;
            Insert = insert;
            Delete = delete;
        }

        public override string ToString()
        {
            return string.Format("{0} @ {4} [{1}:{2};{3}]", Author, FileCount, Insert, Delete, Date);
        }
    }
}
