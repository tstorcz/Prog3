﻿using System.Collections.Generic;

namespace GitLogParser
{
    public class GitData
    {
        private List<Commit> commits = new List<Commit>();

        public int Count
        {
            get { return commits.Count; }
        }

        public Commit this[int index]
        {
            get { return commits[index]; }
            //set { commits[index] = value; }
        }

        public void AddCommit(Commit newCommit)
        {
            commits.Add(newCommit);
        }

        public void Clear()
        {
            commits.Clear();
        }
    }
}
