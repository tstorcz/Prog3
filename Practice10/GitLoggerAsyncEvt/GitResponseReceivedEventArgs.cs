﻿using System;
using static GitLoggerAsyncEvt.GitInfo;

namespace GitLoggerAsyncEvt
{
    class GitResponseReceivedEventArgs : EventArgs
    {
        public string ResponseString { get; private set; }
        public CommandTypes CommandType { get; private set; }

        public GitResponseReceivedEventArgs(CommandTypes commandType, string responseString)
        {
            CommandType = commandType;
            ResponseString = responseString;
        }
    }
}
