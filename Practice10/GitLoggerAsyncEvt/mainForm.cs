﻿using GitLogParser;
using GitLogParser.Model;
using System;
using System.Windows.Forms;

namespace GitLoggerAsyncEvt
{
    public partial class MainForm : Form
    {
        GitInfo gitInfo = new GitInfo();
        GitData gitData = new GitData();

        public MainForm()
        {
            InitializeComponent();

            gitInfo.GitResponseReceived += GitInfo_GitResponseReceived;
            gitInfo.GitInfoClosed += GitInfo_GitInfoClosed;
        }

        private void GitInfo_GitResponseReceived(object sender, GitResponseReceivedEventArgs e)
        {
            switch (e.CommandType)
            {
                case GitInfo.CommandTypes.List:
                    gitData.Clear();
                    Parser.ParseAllByLine(
                        e.ResponseString.Split(new string[] { Environment.NewLine }, StringSplitOptions.None),
                        gitData
                    );
                    commits.Items.Clear();
                    for (int i = 0; i < gitData.Count; i++)
                    {
                        commits.Items.Add(gitData[i]);
                    }

                    this.Invoke(
                        (MethodInvoker)delegate {
                            responseText.Text = e.ResponseString;
                            terminalPage.Enabled = true;
                        }
                        );

                    break;
                case GitInfo.CommandTypes.Details:
                    break;
            }
        }

        private void GitInfo_GitInfoClosed(object sender, EventArgs e)
        {
            this.Invoke(
                        (MethodInvoker)delegate {
                            responseText.Text = "Command process closed";
                        }
                        );
        }

        private void log_Click(object sender, EventArgs e)
        {
            terminalPage.Enabled = false;
            gitInfo.ListCommits();
        }

        private void close_Click(object sender, EventArgs e)
        {
            gitInfo.CloseProcess();

            listCommits.Enabled = false;
            closeProcess.Enabled = false;
            ((Control)gitPage).Enabled = false;
        }
    }
}
