﻿namespace Uszoverseny {
    partial class EredmenyForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rdBtnEredmeny = new System.Windows.Forms.RadioButton();
            this.rdBtnNevsor = new System.Windows.Forms.RadioButton();
            this.btnOrszagok = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtIdoEredmeny = new System.Windows.Forms.TextBox();
            this.txtOrszag = new System.Windows.Forms.TextBox();
            this.txtRajtszam = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnBezar = new System.Windows.Forms.Button();
            this.lstVersenyzok = new System.Windows.Forms.ListBox();
            this.lblCim = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rdBtnEredmeny);
            this.groupBox1.Controls.Add(this.rdBtnNevsor);
            this.groupBox1.Location = new System.Drawing.Point(300, 340);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox1.Size = new System.Drawing.Size(232, 90);
            this.groupBox1.TabIndex = 56;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Rendezési szempont:";
            // 
            // rdBtnEredmeny
            // 
            this.rdBtnEredmeny.AutoSize = true;
            this.rdBtnEredmeny.Location = new System.Drawing.Point(24, 54);
            this.rdBtnEredmeny.Margin = new System.Windows.Forms.Padding(4);
            this.rdBtnEredmeny.Name = "rdBtnEredmeny";
            this.rdBtnEredmeny.Size = new System.Drawing.Size(139, 21);
            this.rdBtnEredmeny.TabIndex = 1;
            this.rdBtnEredmeny.TabStop = true;
            this.rdBtnEredmeny.Text = "Eredmény szerint";
            this.rdBtnEredmeny.UseVisualStyleBackColor = true;
            // 
            // rdBtnNevsor
            // 
            this.rdBtnNevsor.AutoSize = true;
            this.rdBtnNevsor.Location = new System.Drawing.Point(24, 26);
            this.rdBtnNevsor.Margin = new System.Windows.Forms.Padding(4);
            this.rdBtnNevsor.Name = "rdBtnNevsor";
            this.rdBtnNevsor.Size = new System.Drawing.Size(120, 21);
            this.rdBtnNevsor.TabIndex = 0;
            this.rdBtnNevsor.TabStop = true;
            this.rdBtnNevsor.Text = "Névsor szerint";
            this.rdBtnNevsor.UseVisualStyleBackColor = true;
            this.rdBtnNevsor.CheckedChanged += new System.EventHandler(this.rdBtnNevsor_CheckedChanged);
            // 
            // btnOrszagok
            // 
            this.btnOrszagok.Location = new System.Drawing.Point(68, 469);
            this.btnOrszagok.Margin = new System.Windows.Forms.Padding(4);
            this.btnOrszagok.Name = "btnOrszagok";
            this.btnOrszagok.Size = new System.Drawing.Size(163, 28);
            this.btnOrszagok.TabIndex = 55;
            this.btnOrszagok.Text = "Résztvevő országok";
            this.btnOrszagok.UseVisualStyleBackColor = true;
            this.btnOrszagok.Click += new System.EventHandler(this.btnOrszagok_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(296, 210);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(94, 17);
            this.label6.TabIndex = 54;
            this.label6.Text = "Időeredmény:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(296, 164);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(58, 17);
            this.label4.TabIndex = 53;
            this.label4.Text = "Ország:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(296, 121);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(70, 17);
            this.label3.TabIndex = 52;
            this.label3.Text = "Rajtszám:";
            // 
            // txtIdoEredmeny
            // 
            this.txtIdoEredmeny.Location = new System.Drawing.Point(399, 207);
            this.txtIdoEredmeny.Margin = new System.Windows.Forms.Padding(4);
            this.txtIdoEredmeny.Name = "txtIdoEredmeny";
            this.txtIdoEredmeny.ReadOnly = true;
            this.txtIdoEredmeny.Size = new System.Drawing.Size(132, 22);
            this.txtIdoEredmeny.TabIndex = 51;
            // 
            // txtOrszag
            // 
            this.txtOrszag.Location = new System.Drawing.Point(399, 160);
            this.txtOrszag.Margin = new System.Windows.Forms.Padding(4);
            this.txtOrszag.Name = "txtOrszag";
            this.txtOrszag.ReadOnly = true;
            this.txtOrszag.Size = new System.Drawing.Size(132, 22);
            this.txtOrszag.TabIndex = 50;
            // 
            // txtRajtszam
            // 
            this.txtRajtszam.Location = new System.Drawing.Point(399, 117);
            this.txtRajtszam.Margin = new System.Windows.Forms.Padding(4);
            this.txtRajtszam.Name = "txtRajtszam";
            this.txtRajtszam.ReadOnly = true;
            this.txtRajtszam.Size = new System.Drawing.Size(132, 22);
            this.txtRajtszam.TabIndex = 49;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.Location = new System.Drawing.Point(47, 86);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(220, 28);
            this.label2.TabIndex = 48;
            this.label2.Text = "Résztvevők";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnBezar
            // 
            this.btnBezar.Location = new System.Drawing.Point(367, 469);
            this.btnBezar.Margin = new System.Windows.Forms.Padding(4);
            this.btnBezar.Name = "btnBezar";
            this.btnBezar.Size = new System.Drawing.Size(165, 28);
            this.btnBezar.TabIndex = 47;
            this.btnBezar.Text = "Bezár";
            this.btnBezar.UseVisualStyleBackColor = true;
            this.btnBezar.Click += new System.EventHandler(this.btnBezar_Click);
            // 
            // lstVersenyzok
            // 
            this.lstVersenyzok.FormattingEnabled = true;
            this.lstVersenyzok.ItemHeight = 16;
            this.lstVersenyzok.Location = new System.Drawing.Point(47, 121);
            this.lstVersenyzok.Margin = new System.Windows.Forms.Padding(4);
            this.lstVersenyzok.Name = "lstVersenyzok";
            this.lstVersenyzok.Size = new System.Drawing.Size(219, 308);
            this.lstVersenyzok.TabIndex = 46;
            this.lstVersenyzok.SelectedIndexChanged += new System.EventHandler(this.lstVersenyzok_SelectedIndexChanged);
            // 
            // lblCim
            // 
            this.lblCim.Font = new System.Drawing.Font("Garamond", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblCim.ForeColor = System.Drawing.Color.Maroon;
            this.lblCim.Location = new System.Drawing.Point(-4, 35);
            this.lblCim.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCim.Name = "lblCim";
            this.lblCim.Size = new System.Drawing.Size(587, 28);
            this.lblCim.TabIndex = 45;
            this.lblCim.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // EredmenyForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Linen;
            this.ClientSize = new System.Drawing.Size(579, 556);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnOrszagok);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtIdoEredmeny);
            this.Controls.Add(this.txtOrszag);
            this.Controls.Add(this.txtRajtszam);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnBezar);
            this.Controls.Add(this.lstVersenyzok);
            this.Controls.Add(this.lblCim);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "EredmenyForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "EredmenyForm";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton rdBtnEredmeny;
        private System.Windows.Forms.RadioButton rdBtnNevsor;
        private System.Windows.Forms.Button btnOrszagok;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtIdoEredmeny;
        private System.Windows.Forms.TextBox txtOrszag;
        private System.Windows.Forms.TextBox txtRajtszam;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnBezar;
        private System.Windows.Forms.ListBox lstVersenyzok;
        private System.Windows.Forms.Label lblCim;
    }
}