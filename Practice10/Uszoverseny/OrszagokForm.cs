﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace Uszoverseny
{
    public partial class OrszagokForm : Form
    {
        private List<Versenyzo> versenyzok;

        public OrszagokForm(List<Versenyzo> versenyzok)
        {
            InitializeComponent();
            this.versenyzok = versenyzok;
            AddFlags();
        }

        private int bal = 10, fent = 10,
                    tavX = 130, tavY = 117,
                    zaszloX = 120, zaszloY = 90,
                    oszlopSzam = 3;

        private void AddFlags()
        {
            List<String> zaszlok = new List<string>();

            PictureBox flag;
            Label country;

            foreach(Versenyzo v in versenyzok)
            {
                if(!zaszlok.Contains(v.Zaszlo))
                {
                    flag = new PictureBox();
                    flag.Image = Image.FromFile("..\\..\\Files\\" + v.Zaszlo + ".gif");
                    flag.Location = new Point(
                        bal + (zaszlok.Count % oszlopSzam) * tavX, 
                        fent + (zaszlok.Count / oszlopSzam) * tavY
                    );
                    flag.Size = new Size(zaszloX, zaszloY);

                    country = new Label();
                    country.Text = v.Orszag;
                    country.Location = new Point(flag.Location.X, flag.Location.Y + zaszloY);

                    ContentContainer.Controls.Add(flag);
                    ContentContainer.Controls.Add(country);

                    zaszlok.Add(v.Zaszlo);
                }
            }
        }
    }
}
