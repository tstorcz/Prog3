﻿namespace Database_a
{
    partial class mainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.fileName = new System.Windows.Forms.TextBox();
            this.browseInputFile = new System.Windows.Forms.Button();
            this.loadFile = new System.Windows.Forms.Button();
            this.showContent = new System.Windows.Forms.Button();
            this.listBox = new System.Windows.Forms.ListBox();
            this.dataGridView = new System.Windows.Forms.DataGridView();
            this.testConnection = new System.Windows.Forms.Button();
            this.connectionString = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(26, 124);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(82, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "File name:";
            // 
            // fileName
            // 
            this.fileName.Location = new System.Drawing.Point(168, 120);
            this.fileName.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.fileName.Name = "fileName";
            this.fileName.Size = new System.Drawing.Size(712, 26);
            this.fileName.TabIndex = 1;
            // 
            // browseInputFile
            // 
            this.browseInputFile.Location = new System.Drawing.Point(886, 116);
            this.browseInputFile.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.browseInputFile.Name = "browseInputFile";
            this.browseInputFile.Size = new System.Drawing.Size(43, 38);
            this.browseInputFile.TabIndex = 2;
            this.browseInputFile.Text = "...";
            this.browseInputFile.UseVisualStyleBackColor = true;
            this.browseInputFile.Click += new System.EventHandler(this.browseInputFile_Click);
            // 
            // loadFile
            // 
            this.loadFile.Location = new System.Drawing.Point(936, 105);
            this.loadFile.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.loadFile.Name = "loadFile";
            this.loadFile.Size = new System.Drawing.Size(160, 60);
            this.loadFile.TabIndex = 3;
            this.loadFile.Text = "Load File";
            this.loadFile.UseVisualStyleBackColor = true;
            this.loadFile.Click += new System.EventHandler(this.loadFile_Click);
            // 
            // showContent
            // 
            this.showContent.Location = new System.Drawing.Point(29, 199);
            this.showContent.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.showContent.Name = "showContent";
            this.showContent.Size = new System.Drawing.Size(1066, 60);
            this.showContent.TabIndex = 4;
            this.showContent.Text = "Show";
            this.showContent.UseVisualStyleBackColor = true;
            this.showContent.Click += new System.EventHandler(this.showContent_Click);
            // 
            // listBox
            // 
            this.listBox.FormattingEnabled = true;
            this.listBox.ItemHeight = 20;
            this.listBox.Location = new System.Drawing.Point(29, 266);
            this.listBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.listBox.Name = "listBox";
            this.listBox.Size = new System.Drawing.Size(332, 364);
            this.listBox.TabIndex = 5;
            // 
            // dataGridView
            // 
            this.dataGridView.AllowUserToAddRows = false;
            this.dataGridView.AllowUserToDeleteRows = false;
            this.dataGridView.AllowUserToOrderColumns = true;
            this.dataGridView.AllowUserToResizeColumns = false;
            this.dataGridView.AllowUserToResizeRows = false;
            this.dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dataGridView.Location = new System.Drawing.Point(369, 266);
            this.dataGridView.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dataGridView.Name = "dataGridView";
            this.dataGridView.RowHeadersVisible = false;
            this.dataGridView.RowHeadersWidth = 62;
            this.dataGridView.RowTemplate.Height = 24;
            this.dataGridView.Size = new System.Drawing.Size(727, 365);
            this.dataGridView.TabIndex = 6;
            // 
            // testConnection
            // 
            this.testConnection.Location = new System.Drawing.Point(936, 15);
            this.testConnection.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.testConnection.Name = "testConnection";
            this.testConnection.Size = new System.Drawing.Size(160, 60);
            this.testConnection.TabIndex = 7;
            this.testConnection.Text = "Test connection";
            this.testConnection.UseVisualStyleBackColor = true;
            this.testConnection.Click += new System.EventHandler(this.testConnection_Click);
            // 
            // connectionString
            // 
            this.connectionString.Enabled = false;
            this.connectionString.Location = new System.Drawing.Point(168, 31);
            this.connectionString.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.connectionString.Name = "connectionString";
            this.connectionString.Size = new System.Drawing.Size(761, 26);
            this.connectionString.TabIndex = 9;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(26, 35);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(136, 20);
            this.label2.TabIndex = 8;
            this.label2.Text = "Connection String";
            // 
            // mainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1109, 646);
            this.Controls.Add(this.connectionString);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.testConnection);
            this.Controls.Add(this.dataGridView);
            this.Controls.Add(this.listBox);
            this.Controls.Add(this.showContent);
            this.Controls.Add(this.loadFile);
            this.Controls.Add(this.browseInputFile);
            this.Controls.Add(this.fileName);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "mainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Database";
            this.Load += new System.EventHandler(this.mainForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox fileName;
        private System.Windows.Forms.Button browseInputFile;
        private System.Windows.Forms.Button loadFile;
        private System.Windows.Forms.Button showContent;
        private System.Windows.Forms.ListBox listBox;
        private System.Windows.Forms.DataGridView dataGridView;
        private System.Windows.Forms.Button testConnection;
        private System.Windows.Forms.TextBox connectionString;
        private System.Windows.Forms.Label label2;
    }
}

