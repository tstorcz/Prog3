﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Windows.Forms;

//System.Configuration reference has to be added 
//to References section in Solution Explorer

namespace Database_a
{
    public partial class mainForm : Form
    {
        public mainForm()
        {
            InitializeComponent();
        }

        private void mainForm_Load(object sender, EventArgs e)
        {
            //Read ConnectionStrings section of App.config
            //Get specific ConnectionString referenced by name property
            ConnectionStringSettings settings = ConfigurationManager.ConnectionStrings["Persons"];
            connectionString.Text = settings.ToString();
        }

        private void testConnection_Click(object sender, EventArgs e)
        {
            try
            {
                SqlConnection connection = new SqlConnection(connectionString.Text);
                connection.Open();
                connection.Close();
                MessageBox.Show("Connection OK!");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Connection error");
            }
        }

        private void browseInputFile_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog ofd = new OpenFileDialog();
                ofd.FileName = fileName.Text;
                if (ofd.ShowDialog() == DialogResult.OK)
                {
                    fileName.Text = ofd.FileName;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error occurred");
            }
        }

        private void loadFile_Click(object sender, EventArgs e)
        {
            SqlConnection connection = null;
            try
            {
                connection = new SqlConnection(connectionString.Text);
                connection.Open();

                using (StreamReader reader = new StreamReader(fileName.Text))
                {
                    while (!reader.EndOfStream)
                    {
                        string[] words = reader.ReadLine().Split(';');

                        SaveRecord(connection, words);
                    }
                }

                MessageBox.Show("Data imported", "Success");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error occurred");
            }
            finally
            {
                if(connection != null && connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }
        }

        private void showContent_Click(object sender, EventArgs e)
        {
            DataTable personTable = ReadContent();

            listBox.DataSource = personTable;
            listBox.DisplayMember = "last_name";

            dataGridView.DataSource = personTable;
        }

        private void SaveRecord(SqlConnection connection, string[] words)
        {
            SqlCommand cmd = new SqlCommand(
                "INSERT INTO person (first_name, last_name, salary) values (@first_name, @last_name, @salary)", 
                connection
            );

            cmd.Parameters.AddWithValue("@first_name", words[0]);
            cmd.Parameters.AddWithValue("@last_name", words[1]);
            cmd.Parameters.AddWithValue("@salary", int.Parse(words[2]));

            cmd.ExecuteNonQuery();
        }

        private DataTable ReadContent()
        {
            SqlConnection connection = null;
            DataTable personTable = new DataTable();

            try
            {
                connection = new SqlConnection(connectionString.Text);

                SqlCommand cmd = new SqlCommand(
                    "SELECT * FROM person",
                    connection
                );

                connection.Open();

                personTable.Load(cmd.ExecuteReader());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error occurred");
            }
            finally
            {
                if(connection != null && connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }

            return personTable;
        }
    }
}
