﻿using System.Data.Common;

namespace Database
{
    //DbCommand imlementation and SQL command texts are RMDBS dependent,
    //Therefore impelementation of this interface is required to provide RMDBS dependent functionality

    interface IDbConnector
    {
        void Open();
        void Close();

        DbCommand GetCreateCommand();
        DbCommand GetInsertCommand();
        DbCommand GetSelectAllCommand();
    }
}
