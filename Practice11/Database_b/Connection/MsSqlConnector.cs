﻿using System.Data;
using System.Data.Common;
using System.Data.SqlClient;

namespace Database
{
    class MsSqlConnector : IDbConnector
    {
        const string CREATE_TABLE_COMMAND = "CREATE TABLE[dbo].[person] (id int IDENTITY(1,1) NOT NULL, first_name varchar(30) NOT NULL, last_name varchar(30) NOT NULL, salary int NOT NULL)";
        const string INSERT_TABLE_COMMAND = "INSERT INTO person (first_name, last_name, salary) values (@first_name, @last_name, @salary)";
        const string SELECT_TABLE_COMMAND = "SELECT * FROM person";

        SqlConnection connection;

        public MsSqlConnector(string connectionString)
        {
            connection = new SqlConnection(connectionString);
        }

        public void Open()
        {
            connection.Open();
        }

        public void Close()
        {
            connection.Close();
        }

        public DbCommand GetCreateCommand()
        {
            SqlCommand command = new SqlCommand(CREATE_TABLE_COMMAND, connection);

            return command;
        }

        public DbCommand GetInsertCommand()
        {
            SqlCommand command = new SqlCommand(
                INSERT_TABLE_COMMAND,
                connection
            );

            command.Parameters.Add("@first_name", SqlDbType.VarChar);
            command.Parameters.Add("@last_name", SqlDbType.VarChar);
            command.Parameters.Add("@salary", SqlDbType.Int);

            command.Prepare();

            return command;
        }

        public DbCommand GetSelectAllCommand()
        {
            SqlCommand command = new SqlCommand(
                SELECT_TABLE_COMMAND,
                connection
            );

            return command;
        }
    }
}
